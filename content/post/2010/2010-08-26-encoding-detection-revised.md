---
title: Encoding Detection Revised
author: Dominik Haumann

date: 2010-08-26T16:26:43+00:00
url: /2010/08/26/encoding-detection-revised/
categories:
  - Users
tags:
  - planet

---
In recent KDE releases up to version 4.4 Kate unfortunately very often selected the wrong encoding. The result is that e.g. german umlauts (öäü) show up as cryptic signs in the text editor. What I&#8217;ve seen lots of times is that in this case people start to fix those characters manually for the entire document. In other words: They totally do not get at all that the text document simply was opened with the wrong encoding. In fact, the users usually do not even know <a title="Wikipedia: Character Encoding" href="http://en.wikipedia.org/wiki/Character_encoding" target="_blank">what encoding is at all</a>. While this is of course kind of sad, this certainly won&#8217;t change&#8230;

Given this fact, the only correct &#8220;fix&#8221; is a very good automatic encoding detection, such that the encoding is usually chosen correctly. In the rewrite of Kate&#8217;s text buffer for KDE 4.5, Christoph also rewrote the file loader including the encoding detection. The detection now works as follows:

  1. ﻿try selected encoding by the user (through the open-file-dialog or the console)
  2. try encoding detection (some intelligent trial & error method)
  3. use fallback encoding

In step 1, Kate tries to use the encoding specified in the open-file-dialog or the one given when launching Kate from the console. On success, we are done.

The encoding detection in step 2 first tries unicode encoding by looking for a <a title="Wikipedia: Byte Order Mark" href="http://en.wikipedia.org/wiki/Byte_order_mark" target="_blank">Byte Order Mark (BOM)</a>. If found, it is certain that the text document is unicode encoded.  If there is no BOM, Kate next uses a tool from KDElibs (KEncodingProber) to detect the correct encoding. This is basically trial & error: Try encoding A, if there are characters in the document the encoding is not able to represent, try encoding B. Then C and so on&#8230; Unfortunately, this also doesn&#8217;t always work, because a byte sequence might be valid in several encodings and represent different characters. This is why it&#8217;s more or less _impossible_ to get the encoding always right. There is simply no way&#8230;

If the encoding detection fails, Kate uses a fallback encoding. You can configure this fallback encoding in the editor component settings in the &#8220;Open/Save&#8221; category. If the fallback encoding fails as well, the document is marked as read-only and a warning is shown.

### What about Kile and KDevelop?

One of the applications that heavily suffered of the wrong encoding detection in the past was the LaTeX editor Kile. The same holds probably for KDevelop (although it&#8217;s usually less critical with source code). The good news is, that with KDE >= 4.5 the problems with respect to wrong encoding should be gone. So it&#8217;s certainly worth to update if you are affected by this issue.