---
title: Kate – GSoC Summary
author: dianat

date: 2010-08-26T10:43:35+00:00
url: /2010/08/26/kate-gsoc-summary/
categories:
  - Developers
tags:
  - planet

---
Hello planet,

As Google Summer of Code is now finished and I have successfully passed the final evaluation, I would like to give a brief description of my project.

Kate is now able to recover (most of) what was written after last save in case of a crash or power failure. A swap file is created after the first editing action on a document that was successfully saved. If the user closes the document normally or saves its content, the swap file is deleted, otherwise, if Kate crashes, it remains on the disk. On load, Kate searches for the swap file, and if it exists, a warning bar pops from the top and provides the user with three possibilities: recover the lost data, discard the swap file or view differences between the original data and the recovered one. If the user chooses to restore the lost data, the editing actions from the swap file are replayed over the current content of the document. If somehow the swap file is not valid, for example a finishEditing statement is missing, the recovery is done, but the user is warned that it might be incomplete.

Only the core feature for swap file is implemented at the moment. I know I could have done more, but things went slow at the beginning, as I was new to Qt and KDE development and also had a demanding exam period. But this has a positive aspect, too, as will motivate me to continue my work at this project.

This has been a great summer for me as I was accepted into GSoC program and got a chance to do what I like and get paid for it. I want to thank Christoph, my mentor, for having patience with me and helping me with all the problems I have encountered. I also want to thank the whole Kate team, KDE community and Google :).