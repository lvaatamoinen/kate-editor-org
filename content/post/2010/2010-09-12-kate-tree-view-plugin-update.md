---
title: Kate Tree View Plugin Update
author: Thomas Fjellstrom

date: 2010-09-12T15:27:23+00:00
url: /2010/09/12/kate-tree-view-plugin-update/
categories:
  - Users
tags:
  - planet

---
It&#8217;s me again. Given how much people seem to like the plugin, and that some would like to see this plugin replace the existing simple list view, I&#8217;ve put in a bit of work to make it so the tree view plugin replaces as much functionality of the original file list as possible.

There&#8217;s now a list mode (which was surprisingly easy to do), as well as the tree mode and I&#8217;ve extended the sorting support to include all but the &#8220;custom sort order&#8221; option of the original file list (it&#8217;ll take a bit more work to support that, if its something people actually use, I just haven&#8217;t felt like doing the work to get drag and drop to work, and before now it didn&#8217;t make much sense to add). While I was at it I also added a Settings Dialog Page for the plugin.

One improvement (imo) over the old file list, is that all tree view settings are session specific. Each session saves its own sorting, view mode, and item highlighting color settings.

Now for some gratuitous screen shots:

[<img src="/wp-content/uploads/2010/09/kate-treeview-menu2-300x178.png" alt="" width="300" height="178" class="alignnone size-medium wp-image-695" />][1]

Here we see the new sort order context menu items, pretty dull so far.

[<img src="/wp-content/uploads/2010/09/kate-treeview-menu1-300x178.png" alt="" width="300" height="178" class="alignnone size-medium wp-image-695" srcset="/wp-content/uploads/2010/09/kate-treeview-menu1-300x178.png 300w, /wp-content/uploads/2010/09/kate-treeview-menu1-1024x609.png 1024w, /wp-content/uploads/2010/09/kate-treeview-menu1.png 1280w" sizes="(max-width: 300px) 100vw, 300px" />][2]

Now it starts to get a little more interesting. The view mode context menu!

[<img src="/wp-content/uploads/2010/09/kate-treeview-menu3-300x178.png" alt="" width="300" height="178" class="alignnone size-medium wp-image-699" srcset="/wp-content/uploads/2010/09/kate-treeview-menu3-300x178.png 300w, /wp-content/uploads/2010/09/kate-treeview-menu3-1024x609.png 1024w, /wp-content/uploads/2010/09/kate-treeview-menu3.png 1280w" sizes="(max-width: 300px) 100vw, 300px" />][3]

Ooh, and now we can see it displaying in list mode!

[<img src="/wp-content/uploads/2010/09/kate-treeview-menu4-300x178.png" alt="" width="300" height="178" class="alignnone size-medium wp-image-700" srcset="/wp-content/uploads/2010/09/kate-treeview-menu4-300x178.png 300w, /wp-content/uploads/2010/09/kate-treeview-menu4-1024x609.png 1024w, /wp-content/uploads/2010/09/kate-treeview-menu4.png 1280w" sizes="(max-width: 300px) 100vw, 300px" />][4]

Again in list mode, but sorted by the document&#8217;s path, rather than just by document name.

[<img src="/wp-content/uploads/2010/09/kate-treeview-config1-300x161.png" alt="" width="300" height="161" class="alignnone size-medium wp-image-697" srcset="/wp-content/uploads/2010/09/kate-treeview-config1-300x161.png 300w, /wp-content/uploads/2010/09/kate-treeview-config1.png 686w" sizes="(max-width: 300px) 100vw, 300px" />][5]

And here&#8217;s the new config page. Nothing much to look at, but it works :)

I still have a couple more things I want to add, like a fancy way to filter and eventually, if everyone&#8217;s agreeable, I&#8217;ll thief the back/forward actions, and the &#8220;Go&#8221; menu from the main file list.

Before I go, I was wondering if people would be willing to help me with some distro packages, I might be able to manage some Debian packages, but I&#8217;m not sure when I&#8217;ll get to it so if anyone is willing and capable, have at it.

Thats all for now. Thanks for reading :)

 [1]: /wp-content/uploads/2010/09/kate-treeview-menu2.png
 [2]: /wp-content/uploads/2010/09/kate-treeview-menu1.png
 [3]: /wp-content/uploads/2010/09/kate-treeview-menu3.png
 [4]: /wp-content/uploads/2010/09/kate-treeview-menu4.png
 [5]: /wp-content/uploads/2010/09/kate-treeview-config1.png