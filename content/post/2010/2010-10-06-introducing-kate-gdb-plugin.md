---
title: Introducing Kate GDB Plugin
author: Kåre Särs

date: 2010-10-06T21:08:34+00:00
url: /2010/10/06/introducing-kate-gdb-plugin/
categories:
  - Developers
  - Users
tags:
  - planet

---
Have you, like me, had to switch to an external debugger or run raw gdb in the terminal plugin to debug the C/C++ application you develop with Kate? That might not be necessary any more. There is now a &#8220;GDB Plugin&#8221; in trunk that gives you the power of raw GDB with some QAction candy for the most common actions.

I have always been a bit frustrated with not having a debugger directly in Kate, but too lazy/busy to start a debugger plugin project. Fortunately Ian Wakeling started this GDB plugin and got it off the ground. When I got introduced to it at Akademy, it triggered an itch in me to get this plugin into kdesdk and to add some features for the more point and click type of persons. As it seemed I had a bit more free time to spend on the plugin, Ian let me take over as maintainer.

Main features of the plugin:  
&#8211; Jump to the corresponding file and line whenever gdb stops.  
&#8211; Insert and remove breakpoints.  
&#8211; Tool-bar buttons for the most often used actions like step into/over/out. Those actions can of course also be assigned to keyboard shortcuts.  
&#8211; Execution markers and breakpoint markers on the icon border.  
&#8211; A call stack tab for point and click navigation of the call stack.  
&#8211; Optional IO redirection to a separate tab.  
&#8211; Printing the value of the currently selected variable or expression. QStrings and friends are not supported (yet?).  
&#8211; And last but not least. A GDB console for the full power of GDB.

And now the screen-shots  
[<img src="/wp-content/uploads/2010/10/gdb_console-300x171.png" alt="" width="300" height="171" class="alignnone size-medium wp-image-713" srcset="/wp-content/uploads/2010/10/gdb_console-300x171.png 300w, /wp-content/uploads/2010/10/gdb_console.png 967w" sizes="(max-width: 300px) 100vw, 300px" />][1]  
GDB Console, execution marker and a breakpoint marker.

[<img src="/wp-content/uploads/2010/10/gdb_call_stack-300x171.png" alt="" width="300" height="171" class="size-medium wp-image-714" srcset="/wp-content/uploads/2010/10/gdb_call_stack-300x171.png 300w, /wp-content/uploads/2010/10/gdb_call_stack.png 967w" sizes="(max-width: 300px) 100vw, 300px" />][2]  
The current call stack.

[<img src="/wp-content/uploads/2010/10/gdb_settings-300x171.png" alt="" width="300" height="171" class="alignnone size-medium wp-image-716" srcset="/wp-content/uploads/2010/10/gdb_settings-300x171.png 300w, /wp-content/uploads/2010/10/gdb_settings.png 967w" sizes="(max-width: 300px) 100vw, 300px" />][3]  
Settings tab.

[<img src="/wp-content/uploads/2010/10/gdb_redirected_io-300x171.png" alt="" width="300" height="171" class="alignnone size-medium wp-image-717" srcset="/wp-content/uploads/2010/10/gdb_redirected_io-300x171.png 300w, /wp-content/uploads/2010/10/gdb_redirected_io.png 967w" sizes="(max-width: 300px) 100vw, 300px" />][4]  
Redirected IO.

[<img src="/wp-content/uploads/2010/10/gdb_print-value-300x171.png" alt="" width="300" height="171" class="alignnone size-medium wp-image-719" srcset="/wp-content/uploads/2010/10/gdb_print-value-300x171.png 300w, /wp-content/uploads/2010/10/gdb_print-value.png 967w" sizes="(max-width: 300px) 100vw, 300px" />][5]  
The selected variable and the GDB value output.

[<img src="/wp-content/uploads/2010/10/gdb_menu-300x293.png" alt="" width="300" height="293" class="alignnone size-medium wp-image-720" srcset="/wp-content/uploads/2010/10/gdb_menu-300x293.png 300w, /wp-content/uploads/2010/10/gdb_menu.png 468w" sizes="(max-width: 300px) 100vw, 300px" />][6]  
The debug menu.

 [1]: /wp-content/uploads/2010/10/gdb_console.png
 [2]: /wp-content/uploads/2010/10/gdb_call_stack.png
 [3]: /wp-content/uploads/2010/10/gdb_settings.png
 [4]: /wp-content/uploads/2010/10/gdb_redirected_io.png
 [5]: /wp-content/uploads/2010/10/gdb_print-value.png
 [6]: /wp-content/uploads/2010/10/gdb_menu.png