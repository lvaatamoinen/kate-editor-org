---
title: Kate XML Completion Plugin
author: Dominik Haumann

date: 2010-04-27T19:45:00+00:00
url: /2010/04/27/kate-xml-completion-plugin/
blogger_blog:
  - dhaumann.blogspot.com
blogger_author:
  - dhaumannhttp://www.blogger.com/profile/06242913572752671774noreply@blogger.com
blogger_permalink:
  - /2010/04/kate-xml-completion-plugin.html
categories:
  - Developers
  - Users

---
A while ago I&#8217;ve blogged about the Kate&#8217;s most powerful plugins: the <a href="/2010/01/17/kate-xml-completion-plugin-help-wanted/" target="_self">XML completion plugin</a>. Thanks to Tomáš Trnka this plugin is back alive with all its features since today and compiled by default in trunk: It will be included in KDE 4.5. Yay! :) And if you need it already now for KDE 4.4, you can <a href="/2010/04/17/quick-compiling-kate-in-a-stable-kde-environment/" target="_self">build Kate yourself very easily according to this howto</a>, without touching your global KDE installation (probably interesting for KDE documentation writers!). Have fun!