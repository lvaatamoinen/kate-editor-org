---
title: Beware of KDevelop Master and KDELibs 4.5.1 or lower
author: Milian Wolff

date: 2010-09-08T15:57:41+00:00
excerpt: |
  Hey all,
  
  please don&#8217;t use KDevelop master with KDELibs 4.5.1 or lower. Katepart in that version misses a crucial commit that makes KDevelop crash. It is fixed for 4.5.2. In the meantime you have on of the following options:
  
  
  Build kate from sou...
url: /2010/09/08/beware-of-kdevelop-master-and-kdelibs-4-5-1-or-lower-2/
syndication_source:
  - 'Milian Wolff - kate'
syndication_source_uri:
  - http://milianw.de/taxonomy/term/170/0
syndication_source_id:
  - http://milianw.de/tag/kate/feed
"rss:comments":
  - 'http://milianw.de/blog/beware-of-kdevelop-master-and-kdelibs-451-or-lower#comments'
"wfw:commentRSS":
  - http://milianw.de/crss/node/145
syndication_feed:
  - http://milianw.de/tag/kate/feed
syndication_feed_id:
  - "8"
syndication_permalink:
  - http://milianw.de/blog/beware-of-kdevelop-master-and-kdelibs-451-or-lower
syndication_item_hash:
  - f0add455162412e6137a3e8cebe39996
categories:
  - KDE

---
Hey all,

please don&#8217;t use KDevelop master with KDELibs 4.5.1 or lower. Katepart in that version misses a crucial commit that makes KDevelop crash. It is fixed for 4.5.2. In the meantime you have on of the following options:

  1. [Build kate from sources][1]
  2. Switch to the stable branches, i.e.: KDevplatform 1.1, KDevelop 4.1, PHP 1.1, &#8230;
  3. wait for the 4.5.2 release

bye

 [1]: /get-it/