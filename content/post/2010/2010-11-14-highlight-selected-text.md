---
title: Highlight Selected Text
author: Dominik Haumann

date: 2010-11-14T12:49:04+00:00
url: /2010/11/14/highlight-selected-text/
categories:
  - Users
tags:
  - planet

---
<p style="text-align: left;">
  Kate has a new plugin called &#8220;Highlight Selection&#8221;. Since it is a KTextEditor plugin, it is available for all apps using KatePart (e.g.: Kile, Kate, KWrite, KDevelop etc). What does it do? If you select a whole word, all occurrences of this word are highlighted as well:
</p>

<p style="text-align: left;">
  <img class="aligncenter size-full wp-image-761" style="border: 1px solid black;" title="Highlight Selection" src="/wp-content/uploads/2010/11/hl-selection.png" alt="" width="517" height="174" srcset="/wp-content/uploads/2010/11/hl-selection.png 517w, /wp-content/uploads/2010/11/hl-selection-300x100.png 300w" sizes="(max-width: 517px) 100vw, 517px" />
</p>

<p style="text-align: left;">
  You can jump to the next occurrence with ctrl+h (btw: ctrl+h works already since KDE 4.2 or so). The plugin will be included in KDE 4.6. If you can&#8217;t wait, you can use it already now by <a title="Building Kate" href="/get-it/" target="_self">building Kate from the sources</a>.
</p>

<p style="text-align: left;">
  (Side note: Be careful with enabling this plugin in KDevelop, since KDevelop itself is already doing extensive highlighting)
</p>