---
title: 'Kate & Co. on Tour'
author: Christoph Cullmann

date: 2016-06-15T15:19:52+00:00
url: /2016/06/15/kate-co-on-tour/
categories:
  - Common
  - Developers
  - Events
tags:
  - planet

---
We used the nice weather for some small trip near Randa between hacking ;=)  
<img class="aligncenter wp-image-3857 size-full" src="/wp-content/uploads/2016/06/27077238394_423b6b425b_z.jpg" alt="27077238394_423b6b425b_z" width="640" height="426" srcset="/wp-content/uploads/2016/06/27077238394_423b6b425b_z.jpg 640w, /wp-content/uploads/2016/06/27077238394_423b6b425b_z-300x200.jpg 300w" sizes="(max-width: 640px) 100vw, 640px" /> 

But beside getting a bit fresh air, we did other nice things like fixing together with David Faure of Frameworks fame the issue &#8220;how to get automagically icons loaded if you want to bundle them in your installer/app bundle&#8221;, see the change: [&#8220;Add support for loading and using an icontheme in a RCC file automatically.&#8221;][1]

Would be nice if you support us to make things like the Randa sprint possible! ;)  
[<img class="aligncenter" src="https://www.kde.org/fundraisers/randameetings2016/images/banner-fundraising2016.png" width="1400" height="200" />][2]

 [1]: https://phabricator.kde.org/D1878
 [2]: https://www.kde.org/fundraisers/randameetings2016/