    ---
title: RFC - Git Client Integration
author: Dominik Haumann
date: 2019-10-20T19:51:00+02:00
url: /post/2019/2019-10-20-git-client-integration/
---

At this year's KDE conference Akademy we discussed [how to evolve Kate](https://kate-editor.org/post/2019/2019-09-09-kate-planning/) over the next years.
One of the areas we want to improve is better git integration out of the box.
Currently, Kate ships the [Projects plugin](https://kate-editor.org/2012/11/02/using-the-projects-plugin-in-kate/), which automatically detects and loads your file structure from your git repository.
If a project is loaded, then the Search & Replace plugin allows to search&replace in all project files.
In addition, the Quick Open feature also supports opening files from the currently active project - all [explained here](https://docs.kde.org/stable5/en/applications/kate/kate-application-plugin-projects.html).

However, the Projects plugin does not provide any real git integration: You can neither pull nor push, commit, diff, etc.
If at all, additional git functionality is available only via [external tools](https://kate-editor.org/post/2019/2019-09-21-external-tools-plugin/) like gitk or git-cola (e.g. available in the context menu).

This is something we would like to change by having really nice git integration.

### Using Existing Git Clients

Since we don't have the resources to start a git client from scratch, we had a quick look at existing git clients, preferably written in C++ and Qt5.
What we found so far:

#### GitQlient and QGit

Back in 2007, the Qt-based git client [QGit](https://github.com/tibirna/qgit) emerged.
QGit is still alive and maintained.
However, no new major features seem to be added either.
QGit is licensed under GPLv2.

Just some weeks ago, two blogs appeared on [planet.qt.io](https://planet.qt.io/) about [GitQlient](https://github.com/francescmm/GitQlient), which is a fork of QGit.
As discussed in [part 1](https://www.francescmm.com/gitqlient-the-evolution-of-qgit-part-1/), the idea of GitQlient is to have a nice widget that can be embedded into Qt Creator.
This idea is elaborated a bit more in the [follow-up part 2](https://www.francescmm.com/gitqlient-the-new-ui-part-2/).
The GitQlient additions are licensed under LGPLv2+, but given it's based on QGit, the effective license is GPLv2 only for now.

<p align="center">
    <a href="/post/2019/2019-10-20-git-client-integration/images/gitqlient.png" target="_blank"><img src="/post/2019/2019-10-20-git-client-integration/images/gitqlient.png"></a>
</p>

What's nice about GitQlient is that it is Qt5-based and uses just 74 .cpp/.h files, and has only a total amount of 11393 source lines of code (slocs) including API documentation and empty lines.
So it's not that much code, which is good maintenance-wise.
However, both QGit and GitQlient currently are one-man shows, so the developer base is rather small at the moment.

#### gitahead - Understand your Git History

Another Qt5-based git client that caught our attention is [gitahead](https://gitahead.github.io/gitahead.com/).
gitahead seems to be a full-feature git client with many features.
What's very nice about gitahead is: It's licensed under MIT license.
However, the source code is much larger: ~52000 slocs.
In addition, the 3rd party dependencies include e.g. scintilla and many other libs, adding another ~184000 slocs.
Compared to QGit/GitQlient this is 20 times more, and many dependencies we certainly would not want in Kate.

<p align="center">
    <a href="/post/2019/2019-10-20-git-client-integration/images/gitahead.png" target="_blank"><img src="/post/2019/2019-10-20-git-client-integration/images/gitahead.png"></a>
</p>

### Join the Discussion

At this point, nothing is decided yet.
As it currently stands, GitQlient looks a bit more promising, since it's simply much smaller code-wise and therefore likely easier to adapt to Kate.
If we decide to give GitQlient a try, I'd like to contact the initial and current QGit authors to try getting it relicensed from GPLv2 only to GPLv2+ or even LGPLv2+.

If you know of any other Qt-based git clients that can nicely be integrated into Qt/Kate, then please join the discussion at the [KDE subreddit](https://www.reddit.com/r/kde/comments/dkn4gp/rfc_kate_git_client_integration/).
