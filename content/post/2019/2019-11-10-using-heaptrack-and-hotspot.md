---
title: Using Heaptrack and Hotspot
author: Dominik Haumann
date: 2019-11-10T16:55:00+02:00
url: /post/2019/2019-11-10-using-heaptrack-and-hotspot/
---

Some weeks ago at the Open Source Summit & Embedded Linux Conference there was also [a talk by David](https://youtu.be/HOR4LiS4uMI) about using heaptrack and hotspot.
Since these tools are extremely valuable, I thought I'd blog to make these tools a bit more visible in the KDE community.
Have fun watching & happy debugging, and join the discussion [on KDE's subreddit](https://www.reddit.com/r/kde/comments/dud395/using_heaptrack_and_hotspot/) :-)

Resources:

- Video: https://youtu.be/HOR4LiS4uMI
- Slides: https://elinux.org/images/3/30/Tooling.pdf
- [Heaptrack](https://github.com/KDE/heaptrack/), a heap memory profiler for Linux
- [Hotspot](https://github.com/KDAB/hotspot), a Linux perf GUI for performance analysis
