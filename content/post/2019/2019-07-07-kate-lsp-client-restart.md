---
title: Kate LSP Client Restart
author: Christoph Cullmann

date: 2019-07-07T13:10:00+00:00
excerpt: |
  Since my last post about the LSP client progress in May I didn&rsquo;t work on that project at all I think.
  But the good news is, somebody else did scratch that itch on his own ;=)
  We have now a prototype plugin in kate.git master, see lspclient in the...
url: /posts/kate-lsp-client-restart/
enclosure:
  - |
    
    
    
syndication_source:
  - Posts on cullmann.io
syndication_source_uri:
  - https://cullmann.io/posts/
syndication_source_id:
  - https://cullmann.io/posts/index.xml
syndication_feed:
  - https://cullmann.io/posts/index.xml
syndication_feed_id:
  - "11"
syndication_permalink:
  - https://cullmann.io/posts/kate-lsp-client-restart/
syndication_item_hash:
  - ca773cd60bb69dc09eb203c734c725d3
  - 1aac779a9e5f7747d611f4b69d639686
  - 1aac779a9e5f7747d611f4b69d639686
categories:
  - Common

---
Since my last post about the [LSP client progress][1] in May I didn&rsquo;t work on that project at all I think.

But the good news is, somebody else did scratch that itch on his own ;=)

We have now a prototype plugin in kate.git master, see [lspclient][2] in the addons directory.

It is not compiled per default, you can turn it on via:

> cmake -DCMAKE\_INSTALL\_PREFIX=&ldquo;your prefix&rdquo; -DENABLE_LSPCLIENT=ON &ldquo;kate src dir&rdquo;

It shares no code with my initial prototype. The author started this without knowing of my work. This might actually be not that bad, as this plugin is composed of a much smaller code base. This allows to get familiar with the code easier as with the code I copied over from Qt Creator for my initial try.

But, even nicer, it does actually work a lot better than my variant, already now!

What does work (tested with clangd and kate.git/work projects) at the moment:

  * Auto completion: you get the proper LSP server provided completion items <p align="center">
      <a href="https://cullmann.io/posts/kate-lsp-client-restart/images/kate-auto-completion.png" ><img width=500 src="https://cullmann.io/posts/kate-lsp-client-restart/images/kate-auto-completion.png"></a>
    </p>

  * Outline view: Get an extra tool view with the symbols of your current view <p align="center">
      <a href="https://cullmann.io/posts/kate-lsp-client-restart/images/kate-outline-view.png" ><img width=500 src="https://cullmann.io/posts/kate-lsp-client-restart/images/kate-outline-view.png"></a>
    </p>

  * Document highlight: highlight all occurrences of a variable/&hellip; inside the current view <p align="center">
      <a href="https://cullmann.io/posts/kate-lsp-client-restart/images/kate-document-highlight.png" ><img width=500 src="https://cullmann.io/posts/kate-lsp-client-restart/images/kate-document-highlight.png"></a>
    </p>

  * Code navigation: jump to the definition/declaration

There is still a lot of stuff missing and this is all prototype quality. For example the document highlight implementation I added has no way to clear the highlighting at the moment beside document reload.

But given I just needed one hour to add the document highlight support, I would say the code base is easy to adjust.

=> If you have time and want a good LSP client, now you can join the fun and have direct results.

As the author was kind enough to move his work on the plugin to the KDE infrastructure, feel welcome to show up on <kwrite-devel@kde.org> and help out! All development discussions regarding this plugin happen there. We are happy to accept patches, too, if you are a new contributor!

 [1]: https://cullmann.io/posts/kate-lsp-client-progress/
 [2]: https://cgit.kde.org/kate.git/tree/addons/lspclient