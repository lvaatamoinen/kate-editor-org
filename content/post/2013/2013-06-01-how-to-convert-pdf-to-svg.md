---
title: How to convert pdf to svg
author: Dominik Haumann

date: 2013-06-01T14:24:01+00:00
url: /2013/06/01/how-to-convert-pdf-to-svg/
pw_single_layout:
  - "1"
categories:
  - KDE
tags:
  - planet

---
In a project I&#8217;m currently working on I need to display the result of TeX code. So I thought it would be nice and accurate to compile the TeX code to produce a pdf, and then use libpoppler-qt4 to draw the pdf. This works in principal, but there is a licensing problem: libpoppler is GPL, and I want to use it in a LGPL library.

So I searched the net and found <a title="dvipng" href="https://en.wikipedia.org/wiki/Dvipng" target="_blank">dvipng</a>, which converts a dvi file to png. It even supports transparent backgrounds. So I could convert the dvi file to png through QProcess+dvipng, and then display the png file. This works, but whenever you scale the canvas the result looks ugly since png is not a vector graphic.

Next, I found <a title="pdf2svg" href="http://www.cityinthesky.co.uk/opensource/pdf2svg" target="_blank">pdf2svg</a> that converts a pdf file into the scalable vector graphics format svg. In theory, I then can use <a title="QSvgRenderer" href="http://qt-project.org/doc/qt-4.8/qsvgrenderer.html" target="_blank">QSvgRenderer</a> to load and render the SVG on-the-fly using QSvgRenderer::render(QPainter*, &#8230;). So I first tested to convert the pdf to svg with this tool and then view it in InkScape. The result was perfect: InkScape renders the svg file exactly like Okular renders the pdf file:

<img class="aligncenter size-full wp-image-2574" title="pdf2svg" src="/wp-content/uploads/2013/06/pdf2svg.png" alt="" width="530" height="198" srcset="/wp-content/uploads/2013/06/pdf2svg.png 530w, /wp-content/uploads/2013/06/pdf2svg-300x112.png 300w" sizes="(max-width: 530px) 100vw, 530px" /> 

So I looked into the source code of pdf2svg and saw that it uses libpoppler with Cairo. This reflects an additional dependency on cairo, so I went ahead and converted the <a title="Source code of pdf2svg" href="http://www.cityinthesky.co.uk/_media/opensource/pdf2svg-0.2.1.tar.gz" target="_blank">cairo based code</a> to Qt, using libpopper-qt4. The code basically boils down to:

<pre style="padding-left: 30px;">// create poppler pdf document
Poppler::Document *document = Poppler::Document::load(args[1]);
document-&gt;setRenderBackend(Poppler::Document::ArthurBackend);
document-&gt;setRenderHint(Poppler::Document::Antialiasing, true);
document-&gt;setRenderHint(Poppler::Document::TextAntialiasing, true);
document-&gt;setRenderHint(Poppler::Document::TextHinting, true);
document-&gt;setRenderHint(Poppler::Document::TextSlightHinting, true);
document-&gt;setPaperColor(Qt::transparent);</pre>

<pre style="padding-left: 30px;">// prepare QSvgGenerator as QPaintDevice
QSvgGenerator svgGenerator;
svgGenerator.setResolution(72); // resolution in dpi
svgGenerator.setFileName("out.svg");
svgGenerator.setSize(document-&gt;page(0)-&gt;pageSize());

// perform painting
QPainter painter(&svgGenerator);
document-&gt;page(0)-&gt;renderToPainter(&painter);
painter.end();</pre>

This code does indeed generate the svg code for the pdf file. However, the result is surprisingly ugly and wrong:

<img class="aligncenter size-full wp-image-2575" title="pdf2svg-qt4" src="/wp-content/uploads/2013/06/pdf2svg-qt4.png" alt="" width="223" height="154" /> 

First, the pen seems too thick, and second the character &#8216;d&#8217; is wrong on the left. I&#8217;ve tried changing the resolution (setResolution()) and also the page size (setSize()) of QSvgGenerator, always getting the same result. Searching the net reveals that QSvgGenerator seems to have quite some glitches with respect to WYSIWYG rendering. I tried to use QSvgGenerator before in 2009 and came out with unusable results. Maybe I&#8217;m missing something?

**Update 1:** It&#8217;s not QSvgGenerator that is to blame here. It&#8217;s indeed the Arthur backend of libpoppler. I send a patch to the poppler developers that fixes the issue with too bold glyphs. The pahts are still too inaccurate, though.

**Update2:** Using <a title="dvisvgm web page" href="http://dvisvgm.sourceforge.net/" target="_blank">dvisvgm</a> with the option <a title="dvisvgm options" href="http://dvisvgm.sourceforge.net/Manpage" target="_blank">&#8211;no-fonts</a> results in an SVG file that QSvgRenderer renders correctly. So this is the current solution to get a TeX document rendered via SVG in Qt.