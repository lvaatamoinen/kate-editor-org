---
title: Kate in KDE 4.10
author: Dominik Haumann

date: 2013-01-06T15:10:42+00:00
url: /2013/01/06/kate-in-kde-4-10/
pw_single_layout:
  - "1"
categories:
  - Developers
  - Events
  - Users
tags:
  - planet

---
According to our <a title="KDE Release Schedule" href="http://techbase.kde.org/Schedules/KDE4/4.10_Release_Schedule" target="_blank">release schedule</a>, KDE SC 4.10.0 will be available to the public in early February 2013. Following [Kate in KDE 4.7][1], [Kate in KDE 4.8][2] and [Kate in KDE 4.9][3], this blog post highlights what&#8217;s new in Kate in KDE 4.10.

### New Features

  * Kate Part got a [unified notification system][4]. It&#8217;s used in several places already, for instance for [remote file loading][5], [data recovery][6] and Search & Replace information.
  * Kate Part can now be configured to show a [minimap of the document][7] instead of a scrollbar. The minimap shows text in a miniature view and is useful for fast text navigation in the document. While the feature itself is stable, it may be changed and should be considered experimental. Feedback is welcome [on our mailing list][8].
  * Kate Part got several [predefined color schemes][9]. Feel free to contact us with improvements.
  * Kate Part shows the [current line while scrolling][10].
  * Kate got an integrated Quick Open feature (ctrl+alt+o) for fast file navigation.

### News in the Plugins World

<div>
  <ul>
    <li>
      Kate&#8217;s Pate plugin now provides several <a title="Python Plugins" href="/2012/11/22/id-etags-id-etags/">new plugins</a> by default, with <a title="Python 3 Support for Kate" href="/2012/12/02/python-plugin-gets-support-for-python3/">Python 3 support</a>.
    </li>
    <li>
      Kate gained a new and very powerful <a title="Kate Project Plugin" href="/2012/11/02/using-the-projects-plugin-in-kate/">Project plugin</a> with ctags code completion. It is tightly integrated with the Search & Replace plugin as well as the GDB Plugin and the Quick Open feature.
    </li>
    <li>
      Kate Search & Replace plugin gained find-as-you-type support.
    </li>
  </ul>
</div>

### Improvements

<div>
  <ul>
    <li>
      Kate Part&#8217;s <a title="Remove Trailing Spaces" href="/2012/10/27/remove-trailing-spaces/">remove trailing spaces</a> feature is improved.
    </li>
    <li>
      Kate Part&#8217;s <a title="Scripting improvements" href="/2012/11/06/kate-scripting-updates-zen-like-quick-coding/">scripting API gained several new features</a> (e.g. <a title="Zen Coding in Kate Part" href="https://projects.kde.org/projects/kde/kde-baseapps/kate/repository/revisions/a1f58a262f9809ee3300009df1f60ff6af49f058" target="_blank">zen coding script</a>). The API now is more fine grained, e.g. you now need to &#8220;require(&#8220;cursor.js&#8221;);&#8221; in order to have the Cursor API.
    </li>
    <li>
      a lot of other little improvements, see <a title="Kate Bug Tracker" href="https://bugs.kde.org/buglist.cgi?query_format=advanced&product=kate&bug_status=RESOLVED&resolution=FIXED&resolution=WORKSFORME&bugidtype=include&chfieldfrom=2012-08-01&chfieldto=2012-12-31&list_id=377243" target="_blank">our bug tracker</a> for a full list.
    </li>
  </ul>
</div>

### Bug Fixes

Most of the following work was done during the <a title="Akademy 2012" href="http://akademy2012.kde.org/" target="_blank">yearly KDE conference</a> and especially the <a title="Join Kate/KDevelop Meeting in Vienna" href="http://dot.kde.org/2012/11/24/katekdevelop-october-sprint-whats-new-kate" target="_blank">joint Kate/KDevelop meeting</a> this October in Vienna. A massive bug database cleanup was performed mainly by Christoph, so we closed several hundreds of bug reports, where <a title="Kate Bugs Fixed for KDE 4.10" href="https://bugs.kde.org/buglist.cgi?query_format=advanced&product=kate&bug_status=RESOLVED&resolution=FIXED&resolution=WORKSFORME&bugidtype=include&chfieldfrom=2012-08-01&chfieldto=2012-12-31&list_id=377243" target="_blank">~280 are really fixed</a>. So we are down to a total of 400 reports (only 70 of these 400 reports are bugs), where we initially had > 800 open issues. This also is reflected in the Kate bug charts:

[<img class="aligncenter size-full wp-image-2199" title="Kate Bug Tracker" src="/wp-content/uploads/2013/01/bugs.png" alt="" width="652" height="537" srcset="/wp-content/uploads/2013/01/bugs.png 652w, /wp-content/uploads/2013/01/bugs-300x247.png 300w" sizes="(max-width: 652px) 100vw, 652px" />][11]

Thanks to all contributors to make yet another release rock! :-) And as always: We are happy for every contribution, so [check out the Kate sources][12] and send us patches! We hope you enjoy using Kate as much as we enjoy its development :-)

 [1]: /2011/07/09/kate-in-kde-4-7/ "Kate in KDE 4.7"
 [2]: /2011/12/21/kate-in-kde-4-8/ "Kate in KDE 4.8"
 [3]: /2012/06/30/kate-in-kde-4-9/ "Kate in KDE 4.9"
 [4]: /2012/11/06/passive-notifications-in-kate-part/ "Notifications in Kate"
 [5]: /2012/11/05/loading-remote-files/ "Loading Remote Files"
 [6]: /2012/10/25/data-recovery-in-4-10/ "Data Recovery in KDE 4.10"
 [7]: /2012/11/03/busy-katekdevelop-sprint-results-in-mini-map/ "Minimap"
 [8]: /join-us/ "Kate Mailing List"
 [9]: /2012/11/07/default-color-schemas/ "Predefined Color Schemes"
 [10]: /2012/10/28/show-line-while-scrolling/ "Show Current Line while Scrolling"
 [11]: /wp-content/uploads/2013/01/bugs.png
 [12]: /get-it/ "Build Kate"