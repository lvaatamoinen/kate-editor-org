---
title: Qt World Summit + Kate/KDevelop Sprint in Berlin
author: Christoph Cullmann

date: 2015-10-09T08:39:44+00:00
url: /2015/10/09/qt-world-summit-katekdevelop-sprint-in-berlin/
categories:
  - Common
  - Developers
  - Events
  - KDE
  - Users
tags:
  - planet

---
This week is really busy, first three days of Qt World Summit and now hacking away at the Kate/KDevelop sprint in Berlin.

This year my personal goal will be to get our bugs & wishes back into shape.

One major decision I as the maintainer did take was to close all wishes that not got changed since two years. Our team is very small and we have plenty of real bugs to take care of (and their number is rising) such that we will never be able to implement random requests.

If an user is really interested in the feature to come into existance, the wish can be reopened. Still, if no new arguments are brought up that might lead to some more interest by the Kate team, I think that won&#8217;t help a lot. The best solution would be if people could provide patches, which in some cases, like for missing highlighting features in the syntax files, is really easy. At least it should be for the advanced user crowd attracted by an advanced text editor.