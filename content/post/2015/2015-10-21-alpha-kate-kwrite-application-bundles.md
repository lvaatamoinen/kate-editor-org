---
title: 'Alpha Kate & KWrite Application Bundles'
author: Christoph Cullmann

date: 2015-10-21T19:58:02+00:00
url: /2015/10/21/alpha-kate-kwrite-application-bundles/
categories:
  - Developers
  - KDE
  - Users
tags:
  - planet

---
Hi,

with my latest changes in kate.git, icons work, too, on windows and mac, without any qt patch, if you put some qt resource file containing breeze inside your share or Resources folder (one up from the application binary).

<a href="https://quickgit.kde.org/?p=kate.git&a=blob&f=mac.txt" target="_blank">mac.txt</a> contains the rcc calls needed to create such a package and all others things you need to run to get .dmg files. (you need Qt 5.5.0 + a recent CMake + XCode with command line stuff enabled)

Kate plugins should work, too, if you have a qt.conf that points to the local plugins (like macdeployqt will create for you in the mac.txt run).

Things not working ATM without patched Qt or more work: KIO slaves and all dbus related stuff.

With that in place, I consider Kate/KWrite on Mac at least of alpha quality. Still some plugins are missing, optimizations for the .dmg generation could be done like removing stuff we don&#8217;t use like QtWebKit from the bundle, but besides, the programs do something and don&#8217;t crash immediately, at least not on my Mac OS 10.10 installation.

Mandatory Screenshot :=)

[<img class="aligncenter size-medium wp-image-3689" src="/wp-content/uploads/2015/10/kate-mac-300x211.png" alt="Kate on Mac with some plugins" width="300" height="211" srcset="/wp-content/uploads/2015/10/kate-mac-300x211.png 300w, /wp-content/uploads/2015/10/kate-mac-1024x721.png 1024w" sizes="(max-width: 300px) 100vw, 300px" />][1]

Links to Kate & KWrite .dmg files: (**<span style="color: #ff0000;">ALPHA quality</span>** still, only tested on Mac OS 10.10, now just with icons and some plugins and too many Qt libraries and plugins to make the .dmg large :)

[kwrite-alpha-20151021.dmg][2]

[kate-alpha-20151021.dmg][3]

P.S. Testers are welcome, but help of any other kind even more ;) Given Kate/KWrite are trivial to build on a Mac with just XCode around and the stuff described in mac.txt it would be really GREAT if somebody could step up to help to fine tune this to have some more usable beta or even &#8220;real&#8221; release for Mac in the future. I will continue to work on this, but my Mac isn&#8217;t my main working machine nor what I use for my daily company work, therefore, this is not my highest priority.

 [1]: /wp-content/uploads/2015/10/kate-mac.png
 [2]: ftp://cullmann.io/cullmann/kwrite-alpha-20151021.dmg
 [3]: ftp://cullmann.io/cullmann/kate-alpha-20151021.dmg