---
title: 'Akademy 2015 & Kate'
author: Christoph Cullmann

date: 2015-07-31T08:53:34+00:00
url: /2015/07/31/akademy-2015-kate/
categories:
  - Common
  - Developers
  - Events
  - KDE
  - Users
tags:
  - planet

---
Today, I will travel back home from Akademy 2015.

I must say, it was really a nice KDE meeting and I had a lot of fun ;=)  
The first day the KDE e.V. general assembly did take place, then two days of actually interesting talks (including the great announcement of Plasma Mobile). After some more days with interesting BoFs and hacking, Akademy is now ending for me.

I didn&#8217;t do that much work on Kate, I mostly did small bugfixes for the applications bundled with the KDE Applications releases regarding their HiDPI support, finally no Konsole that can&#8217;t redraw correctly on scrolling on a HiDPI screen with scaling activated!

For Kate, the most stuff I did was porting one more plugin (the text filter plugin), fixing some small bugs and rearranging the search bars for in document and in files search. I hope it now is a bit nicer to use, still that is not the final state and I guess we will ask the VDG for more input later on. (btw., if you see any HiDPI glitches in Kate/KTextEditor master, please inform me, I really want to have non-pixelated output ;=)

A big thanks to all organizers & helpers of Akademy 2015! You did a great job, it was a lot of fun, the location was nice, the social event + day trip was good, all fine :) One of the nicer Akademy experiences! Lets hope that the flight home works out, as Dominik is taking the same plane, otherwise the Kate workforce will be seriously diminished.