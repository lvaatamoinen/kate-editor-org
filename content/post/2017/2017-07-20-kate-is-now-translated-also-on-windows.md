---
title: Kate is now translated also on Windows!
author: Kåre Särs

date: 2017-07-20T19:04:47+00:00
url: /2017/07/20/kate-is-now-translated-also-on-windows/
categories:
  - Common

---
This release includes all the feature-enhancements the Linux version has received ([frameworks announcements for 5.36.0][1])

&#8211; Actually working spell-checking.  
&#8211; Possibility to switch interface language.

**EDIT:** Adding an extra fall-back UI language does not wok properly yet.

<figure id="attachment_4052" aria-describedby="caption-attachment-4052" style="width: 783px" class="wp-caption aligncenter">[<img class="size-full wp-image-4052" src="/wp-content/uploads/2017/07/Kate-Sweedish.png" alt="Kate in Sweedish" width="783" height="619" srcset="/wp-content/uploads/2017/07/Kate-Sweedish.png 783w, /wp-content/uploads/2017/07/Kate-Sweedish-300x237.png 300w, /wp-content/uploads/2017/07/Kate-Sweedish-768x607.png 768w" sizes="(max-width: 783px) 100vw, 783px" />][2]<figcaption id="caption-attachment-4052" class="wp-caption-text">Kate in Sweedish</figcaption></figure>

Grab it now at download.kde.org:  [Kate-setup-17.04.3-KF5.36-32bit][3] or [Kate-setup-17.04.3-KF5.36-64bit][4]

 [1]: http://(https://www.kde.org/announcements/kde-frameworks-5.36.0.php
 [2]: /wp-content/uploads/2017/07/Kate-Sweedish.png
 [3]: https://download.kde.org/stable/kate/Kate-setup-17.04.3-KF5.36-32bit.exe
 [4]: https://download.kde.org/stable/kate/Kate-setup-17.04.3-KF5.36-64bit.exe