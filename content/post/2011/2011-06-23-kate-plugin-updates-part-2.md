---
title: Kate plugin updates part 2
author: Kåre Särs

date: 2011-06-23T18:32:15+00:00
url: /2011/06/23/kate-plugin-updates-part-2/
categories:
  - Developers
  - Users
tags:
  - planet

---
Second out in the this series of plugins update is the GDB plugin which has gained a view for local variables. If you have GDB [pretty printes][1] in use, you can even get various Qt types displayed nicely. Here are direct links to the relevant printers and an example [.gdbinit][2]: [qt4.py][3], [libstdcxx.py][4] and [kde4.py][5].

<figure id="attachment_945" aria-describedby="caption-attachment-945" style="width: 736px" class="wp-caption alignnone">[<img class="size-full wp-image-945" src="/wp-content/uploads/2011/06/locals.png" alt="" width="736" height="360" srcset="/wp-content/uploads/2011/06/locals.png 736w, /wp-content/uploads/2011/06/locals-300x146.png 300w" sizes="(max-width: 736px) 100vw, 736px" />][6]<figcaption id="caption-attachment-945" class="wp-caption-text">Locals view</figcaption></figure>

The locals view does not (yet?) have any fancy features as editing the values and does not show which variables have changed.

The handling of breakpoint and execution markers has also improved a bit.

Last but not least in this series will be the new &#8220;Search in files&#8221; plugin.

 [1]: http://nikosams.blogspot.com/2010/01/gdb-qt-pretty-printers-updated.html
 [2]: https://projects.kde.org/projects/extragear/kdevelop/kdevelop/repository/revisions/master/raw/debuggers/gdb/printers/gdbinit
 [3]: https://projects.kde.org/projects/extragear/kdevelop/kdevelop/repository/revisions/master/raw/debuggers/gdb/printers/qt4.py
 [4]: https://projects.kde.org/projects/extragear/kdevelop/kdevelop/repository/revisions/master/raw/debuggers/gdb/printers/libstdcxx.py
 [5]: https://projects.kde.org/projects/extragear/kdevelop/kdevelop/repository/revisions/master/raw/debuggers/gdb/printers/kde4.py
 [6]: /wp-content/uploads/2011/06/locals.png