---
title: The Kate Text Editor - Valentine's Day 2022
author: Christoph Cullmann
date: 2022-02-14T22:31:00+02:00
url: /post/2022/2022-02-14-kate-valentines-day/
---

Once more, Valentine's Day has arrived.

Like [last year](/post/2021/2021-02-14-kate-valentines-day/), Kate's development is nicely progressing this year, too.

## Plenty of new stuff and fixes

We had a very successful year 2021 development wise.

As you can track on our [merge requests page](/merge-requests/) 348 requests got merged directly into Kate and 177 into the KTextEditor editor component.

The year 2022 already began well with 63 patches for Kate and 39 for KTextEditor.

Naturally, more contributions are highly welcome!
If you want to scratch your own itch, please [join us](/join-us/)!

## Feature of the day: Breadcrumb Navigation

Waqar Ahmed introduced some days ago a navigation bar at the top of each view space inside Kate (naturally optional).

![Screenshot of Kate navigation bar visible](/post/2022/2022-02-14-kate-valentines-day/images/kate-navbar-visible.png)

For details of the initial implementation, see [this merge request](https://invent.kde.org/utilities/kate/-/merge_requests/597).

![Screenshot of Kate navigation bar opened](/post/2022/2022-02-14-kate-valentines-day/images/kate-navbar-open.png)

I think this is a great improvement for the typical workflow where you want to e.g. quickly jump between documents close to each other in the directory structure.

## Is this innovative?

Naturally, other editors have very similar features, for example Visual Studio Code has such a breadcrumb navigation, too.
The same is true for many file managers (like Dolphin) or image viewers (like Gwenview), they have this since ages in a similar fashion.

Still I think for Kate this is an innovate new thing to have.
And for sure a lot of people will like it and love to use it.

Therefore, if you read somewhere the time of innovation is over for KDE projects, I object.
Not that I believe in large revolutionary jumps, I have not seen that so far in real life, but I believe we still strive to improve our projects in an innovative fashion.
And often this means incremental improvements and converging into directions already explored by others.

Have a nice Valentine's Day, I hope you had more fun stuff to do today than writing a blog post .P

# Comments?

A matching thread for this can be found here on [r/KDE](https://www.reddit.com/r/kde/comments/ssm3f2/the_kate_text_editor_valentines_day_2022/).
