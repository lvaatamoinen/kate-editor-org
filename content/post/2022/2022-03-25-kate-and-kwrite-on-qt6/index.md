---
title: Kate & KWrite on Qt 6
author: Christoph Cullmann
date: 2022-03-25T20:40:00+02:00
url: /post/2022/2022-03-25-kate-and-kwrite-on-qt6/
---

Given the great work others did already on the [Qt 6 porting of KDE Frameworks](https://www.volkerkrause.eu/2022/03/19/kf6-android-ci-plasma-sprint.html), let's take a look at Kate & KWrite on Qt 6.

With only minor patches, both applications now run on the current master state of KDE Frameworks and Qt 6.2.

Close to all functionality is available, I think the only stripped out part is the hot new stuff upload for snippets and I didn't test the Konsole part.

I would not consider this to be already usable, during trying to type this blog with Qt 6 Kate I got random hangups during completion on Wayland.
That will need debugging.
But for people wanting to improve the Qt 6 experience it is a good enough starting point.

The CI on our GitLab instance will now compile the Qt 6 variant, too, that means we will see compilation regressions.

## Kate on Qt 6.2

![Screenshot of Kate on Qt 6](/post/2022/2022-03-25-kate-and-kwrite-on-qt6/images/kate-on-qt6.webp)

## KWrite on Qt 6.2

![Screenshot of KWrite on Qt 6](/post/2022/2022-03-25-kate-and-kwrite-on-qt6/images/kwrite-on-qt6.webp)

## Help wanted!

You want to help us with making the Qt 6 version a thing you can actually use?

Show up and [contribute](/post/2020/2020-07-18-contributing-via-gitlab-merge-requests/).

# Comments?

A matching thread for this can be found here on [r/KDE](https://www.reddit.com/r/kde/comments/tnxchl/kate_kwrite_on_qt_6/).
