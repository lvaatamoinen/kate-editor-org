---
title: Porting KTextEditor to KSyntaxHighlighting
author: Christoph Cullmann

date: 2018-07-31T19:52:28+00:00
url: /2018/07/31/porting-ktexteditor-to-ksyntaxhighlighting/
categories:
  - Developers
  - KDE

---
After several years, the time has come that KTextEditor finally starts to use more of KSyntaxHighlighting than just the syntax definitions resources.

At the moment, we still do everything on our own (parsing the xml, doing the highlighting, &#8230;) and only use the XML files bundled inside the KSyntaxHighlighting library as &#8220;code sharing&#8221;.

I started a &#8220;syntax-highlighting&#8221; branch in ktexteditor.git to change that. Dominik helped out by starting to add missing API to KSyntaxHighlighting that will ease the porting.

The first step will be: Having full working highlighting without color scheme or configuration support inside the branch.

If that is done, we will take a look at the scheme/configuration stuff.

The current state only handles the highlighting, folding is ATM out of order. It speaks for the quality of Volker&#8217;s KSyntaxHighlighting implementation that with minimal additions only 1-2 hours of work did lead to a &#8220;usable&#8221; result.

=> obligatory screenshot

<img class="aligncenter size-full wp-image-4153" src="/wp-content/uploads/2018/07/kwrite_ksyntaxhighlighting.png" alt="" width="812" height="807" srcset="/wp-content/uploads/2018/07/kwrite_ksyntaxhighlighting.png 812w, /wp-content/uploads/2018/07/kwrite_ksyntaxhighlighting-150x150.png 150w, /wp-content/uploads/2018/07/kwrite_ksyntaxhighlighting-300x298.png 300w, /wp-content/uploads/2018/07/kwrite_ksyntaxhighlighting-768x763.png 768w" sizes="(max-width: 812px) 100vw, 812px" /> 

Beside the missing folding markers, it doesn&#8217;t look that bad, or?

I hope we can work on that during Akademy, too, to finally move on and not keep all things duplicated just because we got no time to work on it.

Btw., thanks again to Volker for the work on the framework!