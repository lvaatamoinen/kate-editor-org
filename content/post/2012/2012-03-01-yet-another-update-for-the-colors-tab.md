---
title: Yet another update for the Colors tab
author: Dominik Haumann

date: 2012-03-01T19:07:13+00:00
url: /2012/03/01/yet-another-update-for-the-colors-tab/
pw_single_layout:
  - "2"
categories:
  - Developers
  - Users
tags:
  - planet

---
Now, the categories in the &#8220;Colors&#8221; tab are painted like in System Settings (top: new, bottom: old):

[<img class="size-full wp-image-1721 aligncenter" title="Final Colors Tab" src="/wp-content/uploads/2012/03/color-final.png" alt="" width="782" height="560" srcset="/wp-content/uploads/2012/03/color-final.png 782w, /wp-content/uploads/2012/03/color-final-300x214.png 300w" sizes="(max-width: 782px) 100vw, 782px" />][1]

<p style="text-align: center;">
  <a href="/wp-content/uploads/2012/03/colors-new.png"><img class="alignnone size-full wp-image-1712" title="New Color Tab" src="/wp-content/uploads/2012/03/colors-new.png" alt="" width="904" height="560" srcset="/wp-content/uploads/2012/03/colors-new.png 904w, /wp-content/uploads/2012/03/colors-new-300x185.png 300w" sizes="(max-width: 904px) 100vw, 904px" /></a>
</p>

<p style="text-align: left;">
  Thanks to Rafael Fernández López for relicensing the code of SystemSettings&#8217; CategoryDrawer under LGPLv{2,3}.
</p>

 [1]: /wp-content/uploads/2012/03/color-final.png