---
title: Project Management, Take Two
author: Christoph Cullmann

date: 2012-08-06T19:38:30+00:00
url: /2012/08/06/project-management-take-two/
pw_single_layout:
  - "1"
categories:
  - Common
  - Developers
  - KDE
tags:
  - planet

---
After a bit toying around, the first version of project management plugin has landed in the master branch of the kate.git.

First time I developed a new feature in a feature-clone in my scratch space on git.kde.org. Very cool, KDE sysadmins did a GREAT job with that stuff: easy to setup and use ;)

Now, people wanting to try out that, just clone our kate.git and [compile][1] the master branch.

Example project is toplevel in kate.git: kate.kateproject

To load it, open Kate, activate the &#8220;Project Plugin&#8221; and hit the &#8220;Open Project&#8221; button in your main menu bar and go to the &#8220;Projects&#8221; tool view on the left.

No, it won&#8217;t auto-raise atm and yeah, the button looks awful :P

And here the mandatory screenshot:

[<img class="aligncenter size-medium wp-image-1936" title="Kate Project Plugin" src="/wp-content/uploads/2012/08/kateproject-300x264.png" alt="" width="300" height="264" srcset="/wp-content/uploads/2012/08/kateproject-300x264.png 300w, /wp-content/uploads/2012/08/kateproject.png 844w" sizes="(max-width: 300px) 100vw, 300px" />][2]

It still is not much, but I am happy with the result as this is first time since long for me to code something with model-view and Qt.

 [1]: /get-it/
 [2]: /wp-content/uploads/2012/08/kateproject.png