---
title: Bug Charts on bko
author: Dominik Haumann

date: 2012-07-23T13:52:01+00:00
url: /2012/07/23/bug-charts-on-bko/
pw_single_layout:
  - "1"
categories:
  - Developers
tags:
  - planet

---
Since the upgrade on bko, the bug charts were a bit broken. Thanks to our awesome sysadmins, this is fixed now: <a title="Kate Bugs" href="https://bugs.kde.org/reports.cgi?product=kate&output=show_chart&datasets=ASSIGNED&datasets=REOPENED&datasets=UNCONFIRMED&datasets=NEW" target="_blank">Example for Kate</a>. In case you notice any wrong behavior, please let the sysadmins know immediately! Thanks :)