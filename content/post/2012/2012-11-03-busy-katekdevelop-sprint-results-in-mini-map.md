---
title: Busy Kate/KDevelop Sprint results in Mini-Map
author: Kåre Särs

date: 2012-11-03T11:08:57+00:00
url: /2012/11/03/busy-katekdevelop-sprint-results-in-mini-map/
pw_single_layout:
  - "1"
categories:
  - Common

---
One of the results of the Kate/KDevelop sprint in Vienna is the new &#8220;MiniMap&#8221; that can be used instead of the vertical scrollbar. I came to the sprint with a black and white version of it for the symbols viewer, but got encouraged to integrate it to the core. The result, in cooperation with Sven Brauch from KDevelop, is a nice Mini-Map that can be enabled both in Kate and KDevelop.

And to make this into a real blog and to provide proof, here are a couple of screenshots:

<figure id="attachment_2109" aria-describedby="caption-attachment-2109" style="width: 971px" class="wp-caption alignnone">[<img class="size-full wp-image-2109" src="/wp-content/uploads/2012/11/MiniMap.png" alt="" width="971" height="734" srcset="/wp-content/uploads/2012/11/MiniMap.png 971w, /wp-content/uploads/2012/11/MiniMap-300x226.png 300w" sizes="(max-width: 971px) 100vw, 971px" />][1]<figcaption id="caption-attachment-2109" class="wp-caption-text">The MiniMap enabled</figcaption></figure>

<figure id="attachment_2110" aria-describedby="caption-attachment-2110" style="width: 768px" class="wp-caption alignnone">[<img class="size-full wp-image-2110" src="/wp-content/uploads/2012/11/MiniMapOption.png" alt="" width="768" height="546" srcset="/wp-content/uploads/2012/11/MiniMapOption.png 768w, /wp-content/uploads/2012/11/MiniMapOption-300x213.png 300w" sizes="(max-width: 768px) 100vw, 768px" />][2]<figcaption id="caption-attachment-2110" class="wp-caption-text">Here is where/how you turn it on in Kate</figcaption></figure>

 [1]: /wp-content/uploads/2012/11/MiniMap.png
 [2]: /wp-content/uploads/2012/11/MiniMapOption.png