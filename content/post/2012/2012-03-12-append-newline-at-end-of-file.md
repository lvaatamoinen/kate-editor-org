---
title: Append Newline at End of File
author: Dominik Haumann

date: 2012-03-12T21:20:58+00:00
url: /2012/03/12/append-newline-at-end-of-file/
pw_single_layout:
  - "1"
categories:
  - Users
tags:
  - planet

---
In KDE SC 4.9, Kate Part will have an option in the Open/Save config tab called

<pre style="padding-left: 30px;">[ ] Append newline at end of file on save</pre>

By default (and Kate tradition), this option is off. You can also use the <a href="http://docs.kde.org/stable/en/kde-baseapps/kate/config-variables.html" target="_blank">document variable (modeline)</a> newline-at-eof [bool], either in the file itself, in a .kateconfig file, or in the &#8220;Modes & Filetypes&#8221; config page. If a newline was added, it is visible only after reloading the document. This finally fixes <a title="Kate Wish #256134" href="https://bugs.kde.org/show_bug.cgi?id=256134" target="_blank">wish #256134</a>.