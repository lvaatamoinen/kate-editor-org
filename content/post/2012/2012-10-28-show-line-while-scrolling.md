---
title: Show Line while Scrolling
author: Dominik Haumann

date: 2012-10-28T01:56:35+00:00
url: /2012/10/28/show-line-while-scrolling/
pw_single_layout:
  - "1"
categories:
  - Developers
  - Users
tags:
  - planet

---
Another quick update: Jowenn just implemented a decent tool tip that displays the current view line while scrolling, a [wish from 9 years ago][1]. Mandatory screenshot (pimped with my &#8220;awesome&#8221; Gimp skills):

<img class="aligncenter size-full wp-image-2067" title="Show Current Scroll Line" src="/wp-content/uploads/2012/10/scroll.png" alt="" width="530" height="319" srcset="/wp-content/uploads/2012/10/scroll.png 530w, /wp-content/uploads/2012/10/scroll-300x180.png 300w" sizes="(max-width: 530px) 100vw, 530px" />

 [1]: https://bugs.kde.org/show_bug.cgi?id=61335 "Show Scroll Line"