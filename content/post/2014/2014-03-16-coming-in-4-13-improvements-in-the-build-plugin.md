---
title: 'Coming in 4.13: Improvements in the build plugin'
author: Alexander Neundorf

date: 2014-03-16T21:51:08+00:00
url: /2014/03/16/coming-in-4-13-improvements-in-the-build-plugin/
categories:
  - Common
  - Developers
  - KDE
  - Users

---
Kate comes with a build plugin, which supports running make, or <a href="http://martine.github.io/ninja/" target="_blank">ninja</a>,  or actually any arbitrary command directly from within Kate. This is obvisouly  useful when using Kate as development editor, and this plugin has seen several improvements for the 4.13 release.

A small change, but for affected developers a major improvement, is that Kate  can now parse warning and error messages from the Intel compilers, icpc and icc.  
So for those of you using icpc, Kate can now automatically jump to the line of code which caused the error. Actually you don&#8217;t have to wait for 4.13 for this, it is already available since 4.12.3.

Beside that, there are improvements which benefit all users of the build plugin. Let&#8217;s start with a screenshot, which already shows one of the major changes.

<figure id="attachment_3245" aria-describedby="caption-attachment-3245" style="width: 660px" class="wp-caption aligncenter">[<img class=" wp-image-3245    " alt="kate-targets" src="/wp-content/uploads/2014/03/kate-targets-300x220.png" width="660" height="484" />][1]<figcaption id="caption-attachment-3245" class="wp-caption-text">Build plugin showing the list of targets and the Build-menu</figcaption></figure>

&nbsp;

Up to 4.12, it was possible to create multiple &#8220;targets&#8221;, and each of these targets could contain a &#8220;Build&#8221; command, a &#8220;Clean&#8221; command and a &#8220;Quick&#8221; command.  As of 4.13, the build plugin is not limited anymore to these three commands. As can be seen in the screenshot, every &#8220;target set&#8221; can now  contain an arbitrary number of actual targets.  
They are listed in a table widget. To add a target, click the green &#8220;plus&#8221;-button in the lower right. This will  append a target to the end of the list. It can be edited by simply double clicking it (or pressing F2 while keyboard focus is in the table).  
To delete a target, click the &#8220;minus&#8221;-button right next to the &#8220;plus&#8221;  
One of the targets can be marked as the &#8220;default&#8221; target. This will typically be  the target which runs &#8220;make all&#8221;. Additionally one target can be marked  as the &#8220;clean&#8221; target, this will typically be &#8220;make clean&#8221;. For these two special  targets separate keyboard shortcuts can be assigned, so they are always  quickly available.

A whole set of actions which can be bound to shortcuts can be seen here:

<figure id="attachment_3246" aria-describedby="caption-attachment-3246" style="width: 320px" class="wp-caption aligncenter">[<img class=" wp-image-3246    " alt="build-shortcuts" src="/wp-content/uploads/2014/03/build-shortcuts-300x215.png" width="320" height="231" />][2]<figcaption id="caption-attachment-3246" class="wp-caption-text">Configuring keyboard shortcuts for the build plugin</figcaption></figure>

&nbsp;

Now to the actually interesting part: building something.  
There are multiple ways how to start building a target.

The default and the clean targets can be built directly using keyboard shortcuts, in the screenshot above I assigned F8 to the default target.  
To build another than the default target, you can select the target you want in the table and then build it by clicking the blue &#8220;check&#8221;-button next to the &#8220;plus&#8221; and &#8220;minus&#8221; buttons.  
For keyboard users, there is a quick-select dialog. It shows a list with the names of all targets, which can be filtered by typing part of the target name. That&#8217;s a really quick way to build any of the available targets. Here&#8217;s a screenshot:

<figure id="attachment_3247" aria-describedby="caption-attachment-3247" style="width: 660px" class="wp-caption aligncenter">[<img class="wp-image-3247  " alt="kate-targets-quick-select" src="/wp-content/uploads/2014/03/kate-targets-quick-select-300x220.png" width="660" height="484" />][3]<figcaption id="caption-attachment-3247" class="wp-caption-text">The Quick-select dialog for building a target</figcaption></figure>

&nbsp;

Once building has started, the output is displayed in the log view.  
As can be seen, there is only one output tab left, where the &#8220;level of detail&#8221; can be adjusted using a slider. While building, the plugin automatically switches to the log display.

<figure id="attachment_3248" aria-describedby="caption-attachment-3248" style="width: 660px" class="wp-caption aligncenter">[<img class=" wp-image-3248  " alt="kate-build-log" src="/wp-content/uploads/2014/03/kate-build-log-300x235.png" width="660" height="517" srcset="/wp-content/uploads/2014/03/kate-build-log-300x235.png 300w, /wp-content/uploads/2014/03/kate-build-log-1024x802.png 1024w, /wp-content/uploads/2014/03/kate-build-log.png 1053w" sizes="(max-width: 660px) 100vw, 660px" />][4]<figcaption id="caption-attachment-3248" class="wp-caption-text">Build plugin showing the output while building.</figcaption></figure>

&nbsp;

Also new, there is now a simple status display, which tells you which target is currently being built or was built previously.  
Next to it, there is yet another way to start a build, the &#8220;Build again&#8221; button. Once some target has been built, using this button the same target can be built again. Oh, and there is now also a button to cancel a build, in case you forgot the assigned keyboard shortcut.

When building has finished, the output tab automatically switches to a parsed output mode, which lists the warning &#8211; and error messages. By double clicking on one of them or using the keyboard shortcut of your choice, I assigned F9,  you can jump directly to the line of code which caused the error.

<figure id="attachment_3249" aria-describedby="caption-attachment-3249" style="width: 660px" class="wp-caption aligncenter">[<img class=" wp-image-3249  " alt="kate-build-error" src="/wp-content/uploads/2014/03/kate-build-error-300x226.png" width="660" height="498" srcset="/wp-content/uploads/2014/03/kate-build-error-300x226.png 300w, /wp-content/uploads/2014/03/kate-build-error-1024x773.png 1024w, /wp-content/uploads/2014/03/kate-build-error.png 1062w" sizes="(max-width: 660px) 100vw, 660px" />][5]<figcaption id="caption-attachment-3249" class="wp-caption-text">Build plugin showing the parsed errors after the build failed.</figcaption></figure>

All that together, should make the build plugin even more useful than before.  
Have fun compiling ! :-)

Alex

 [1]: /wp-content/uploads/2014/03/kate-targets.png
 [2]: /wp-content/uploads/2014/03/build-shortcuts.png
 [3]: /wp-content/uploads/2014/03/kate-targets-quick-select.png
 [4]: /wp-content/uploads/2014/03/kate-build-log.png
 [5]: /wp-content/uploads/2014/03/kate-build-error.png