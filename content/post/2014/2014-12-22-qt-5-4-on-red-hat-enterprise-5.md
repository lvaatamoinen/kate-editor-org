---
title: Qt 5.4 on Red Hat Enterprise 5
author: Christoph Cullmann

date: 2014-12-22T19:25:44+00:00
url: /2014/12/22/qt-5-4-on-red-hat-enterprise-5/
categories:
  - Developers
tags:
  - planet

---
For my job, I need to take care of the support of old Linux distributions for our products, therefore I experimented in building Qt 5.x for Red Hat Enterprise 5 (or CentOS 5 or other clones).

Whereas Red Hat Enterprise 6 works more or less out of the box, to build Qt (even with WebKit and Co.) on Red Hat Enterprise 5, more work is needed. Even the xcb library is not yet existent there.

Therefore, here a small howto, for other people having problems with building it. It contains some hacks and any input is for sure nice to have as an comment (to help others to avoid my faults).

Disclaimer: I am not responsible for creating a completely messy and buggy Qt 5.x build :=)

First, lets assume you have some CentOS 5.11 box or chroot.

You will need some new development toolsets to get rolling:

wget http://people.centos.org/tru/devtools-1.1/devtools-1.1.repo -O /etc/yum.repos.d/devtools-1.1.repo  
wget http://people.centos.org/tru/devtools-2/devtools-2.repo -O /etc/yum.repos.d/devtools-2.repo

I tend to use the version 2, version 1.1 does work, too (GCC 4.7.2 vs. 4.8.2).

We will needed EPEL, for a more up-to-date python (2.6, for xcb):

wget http://ftp.tu-chemnitz.de/pub/linux/fedora-epel/5/x86_64/epel-release-5-4.noarch.rpm  
rpm -i epel-release-5-4.noarch.rpm

Then, lets install the minimal deps (I install both toolsets, you can choose one):

yum install devtoolset-1.1 devtoolset-2 python26 fontconfig-devel libX11-devel libXau-devel libXext-devel libXrender-devel

Then, get some compile shell ready:

scl enable devtoolset-2 bash

I will just for excercise build Qt and stuff into /tmp/usr/(qt)?

To build Qt, you need libxcb, get that (with deps):

\# package config for xcb stuff  
export PKG\_CONFIG\_PATH=/tmp/usr/lib/pkgconfig

\# xcb proto  
wget http://xcb.freedesktop.org/dist/xcb-proto-1.11.tar.gz  
tar -xzf xcb-proto-1.11.tar.gz  
cd xcb-proto-1.11  
./configure &#8211;prefix=/tmp/usr  
make -j8  
make install  
cd .. 

\# pthread stubs  
wget http://xcb.freedesktop.org/dist/libpthread-stubs-0.3.tar.gz  
tar -xzf libpthread-stubs-0.3.tar.gz  
cd libpthread-stubs-0.3  
./configure &#8211;prefix=/tmp/usr  
make -j8  
make install  
cd ..

\# xcb  
wget http://xcb.freedesktop.org/dist/libxcb-1.11.tar.gz  
tar -xzf libxcb-1.11.tar.gz  
cd libxcb-1.11  
./configure &#8211;prefix=/tmp/usr  
make -j8  
make install  
cd ..

Now we are ready to build Qt 5.4, at least some parts ;=)  
I skip here the GL stuff, I don&#8217;t need it and I skip some other modules.

\# unpack qt  
tar -xzf qt-everywhere-opensource-src-5.4.0.tar.gz  
cd qt-everywhere-opensource-src-5.4.0

\# no perf events => patch that out, won&#8217;t compile, at least not for me because of missing syscall/broken kernel header  
sed -i &#8220;s/#define QTESTLIB\_USE\_PERF\_EVENTS/#undef QTESTLIB\_USE\_PERF\_EVENTS/g&#8221; qtbase/src/testlib/qbenchmark_p.h

\# configure with some defines to work on centos 5 (fake some defines)  
./configure -R &#8216;\\\$$ORIGIN&#8217; -D \_X\_INLINE=inline -D XK\_dead\_currency=0xfe6f -D XK\_ISO\_Level5\_Lock=0xfe13 -D FC\_WEIGHT\_EXTRABLACK=215 -D FC\_WEIGHT\_ULTRABLACK=FC\_WEIGHT_EXTRABLACK -v -opensource -confirm-license -sysconfdir /etc/xdg -prefix /tmp/usr/qt -release -shared -qt-zlib -qt-libpng -qt-libjpeg -qt-pcre -qt-xcb -qt-xkbcommon -xkb-config-root /usr/share/X11/xkb -no-xcb-xlib -c++11 -nomake examples -nomake tests -no-dbus -no-icu -no-opengl -skip activeqt -skip androidextras -skip connectivity -skip enginio -skip location -skip macextras -skip multimedia -skip quick1 -skip sensors -skip serialport -skip wayland -skip webchannel -skip webengine -skip webkit -skip webkit-examples -skip websockets -skip winextras -skip x11extras

\# build and install  
make -j8  
make install

=> You have some Qt, but keep in mind you need to bundle at least the libxcb.so* library, as your Red Hat Enterprise 5.x guys won&#8217;t have it!