---
title: Contributing via GitLab Merge Requests
author: Christoph Cullmann
date: 2020-07-18T19:53:00+02:00
url: /post/2020/2020-07-18-contributing-via-gitlab-merge-requests/
---

[KDE](https://www.kde.org) switched over to a self-hosted [GitLab](https://about.gitlab.com/) instance this year: [invent.kde.org](https://invent.kde.org).
Many thanks again to our system administrators and all other contributors to this large effort!

This means, e.g. Kate, KTextEditor and KSyntaxHighlighting are now using the GitLab workflow for all contributions.
Whereas there are still some old review requests and tasks on [phabricator.kde.org](https://phabricator.kde.org), all new contributions should arrive as merge requests on invent.kde.org.

For the three projects I take care of, you can see what already arrived and is now under discussion or already merged here:

* [KSyntaxHighlighting - Merge Requests](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests)
* [KTextEditor - Merge Requests](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests)
* [Kate - Merge Requests](https://invent.kde.org/utilities/kate/-/merge_requests)

Even this website is located there, regenerated via [HUGO](https://gohugo.io/) every few minutes on the web server hosting it.
If you want to write some post about Kate or update some of our pages, please just submit a request here:

* [kate-editor.org - Merge Requests](https://invent.kde.org/websites/kate-editor-org/-/merge_requests)

I think the current setup is really superior to what we had before.
For example, Kate already has now [79 merge requested accepted](https://invent.kde.org/utilities/kate/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged).
It was never easier to contribute than now.

I apologize that sometimes the review speed for merge requests is still lacking, I hope this improves with more people taking care in the development process.
But I reelly encourage people that can code to help us out by contributing fixes + features!

We have a rather long backlog of bugs and wishes that are not really taken care of as it should, see the below lists:

* [Kate & KTextEditor Bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&list_id=1770602&product=frameworks-ktexteditor&product=kate)
* [Kate & KTextEditor Wishes](https://bugs.kde.org/buglist.cgi?bug_severity=wishlist&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&list_id=1770603&product=frameworks-ktexteditor&product=kate)

We really would appreciate help with this, give it some try.

You want to contribute a new LSP server config? Easy, see [this accepted merge request](https://invent.kde.org/utilities/kate/-/merge_requests/93) as template.

You want to improve the UI by e.g. having a more "normal" tab bar instead of our LRU variant? Submit a request like [this](https://invent.kde.org/utilities/kate/-/merge_requests/89).

It might look in the bug tracker like we are not eager to improve or accept changes, that is not true, we are just not that free to work a lot on our own on larger things, as the team is small and we don't have that much spare time left to handle it.
But if somebody steps up to do it, we will help out to get it in, as can be seen by the already merged stuff.

Still, there might be discussions about what shall go in or not.
Please be open to accept the feedback we give and better start early to ask for some, as that might avoid wasted effort on your side.

I think the new infrastructure makes it easier for people to step up and do some work, I really like that.
I hope a nice stream of merge requests keep coming in ;=)
At least for the projects mentioned above I think scratching your own itch was never easier.
