---
title: Kate is soon 20 years old!
author: Christoph Cullmann
date: 2020-11-08T14:56:00+02:00
url: /post/2020/2020-11-08-kate-is-soon-20-years-old/
---

## 20 years...

As described [here](/2010/08/15/kate-history/) Kate will soon be twenty.

My mail to the former KWrite maintainer dates back to "Thu, 14 Dec 2000 18:38:42 +0100", thought I think I started earlier with actually hacking on that MDI KWrite variant.

But just to have some official date, let's say Kate was born at 14th December 2000, in a bit over one month Kate will turn officially twenty ;=)

## Kate's state around 2000

We started off with something like below (the screenshot is a bit fresher, it's from 2001, KDE 2.2.x):

<p align="center">
    <a href="/wp-content/uploads/2011/08/kate-in-kde2.2.png" target="_blank"><img width=700 src="/wp-content/uploads/2011/08/kate-in-kde2.2.png"></a>
</p>

I think we already had the basics that we still have around today: some split-able main area for the documents and tool-views for stuff like a document list or terminal.
We had no tabbed document switching, by design.

## Kate's state 20 years later

And now, in 2020, we ended up here:

<p align="center">
    <a href="/post/2020/2020-11-08-kate-is-soon-20-years-old/images/kate-2020.png" target="_blank"><img width=700 src="/post/2020/2020-11-08-kate-is-soon-20-years-old/images/kate-2020.png"/></a>
</p>

That means we spend 20 years just to change the color theme to something dark and add some tabs ;=)
Not bad, isn't it?

Fun aside, the screenshots show: a lot of the basics did stay the same over the 20 years.

But that doesn't mean that not a lot of the underlying stuff changed drastically.

We amassed a lot of features over that time period, too.

The original Kate/KWrite/KTextEditor/... had by far not the syntax highlighting we have today or any kind of working auto indentation, it had not that many nifty editing actions, you could only dream from [decent code completion](/post/2020/2020-01-01-kate-lsp-client-status/).

And I would not have thought that it ever really [works well on Windows](/post/2020/2020-09-28-kate-windows-store-current-status/).

## How will it continue?

How the project will continue depends on the contribution we get.

I am very happy with the progress of the last few years.

We gained more traction again and with the switch to our GitLab instance we got a lot of contributions merged in the past months.

If you have some itch to scratch, step up and provide a patch!

Look at what we got merged already since we are at [invent.kde.org](https://invent.kde.org):

* 103 patches for [Kate](https://invent.kde.org/utilities/kate/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
* 37 patches for [KTextEditor](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
* 95 patches for [KSyntaxHighlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)

I think this is nice and I hope this continues that way (or even speeds up).

Even after 20 years, Kate is not dead nor abandoned.

I think that is a nice achievement by all people that contribute now and in the past to our stuff.

Unfortunately not a lot people that helped to start this have stayed with the project, but that is life, priorities shift, jobs change, ...

;=) Not everybody can be a loner that has time to maintain something for two decades.

## Thanks!

I want to thank all contributors!

Even [this incomplete auto-generated list](/the-team/) contains more than 500 people that contributed stuff in the past 20 years.

Thanks that you helped to enhance Kate and keep it alive and kicking for two decades.

And I hope Kate will continue to be well for the near and far future ;=)

Let's provide some viable text editor alternative for users out there, be it on some Linux/BSD/... system or Windows.

And to our users: Thanks for picking Kate as your editor!
