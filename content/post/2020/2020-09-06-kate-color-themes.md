---
title: Kate - Color Themes
author: Christoph Cullmann
date: 2020-09-06T19:45:00+02:00
url: /post/2020/2020-09-06-kate-color-themes/
---

### Color Themes aka "Schemas"

KTextEditor (and therefore Kate/KWrite/KDevelop/...) has a concept of color themes since more than one decade.

In the KTextEditor UI this is called a "Schema" at the moment.
(Don't ask me why it wasn't called color theme or something like this, seems I wasn't that clever at choosing names in the past.)

The user can configure the colors inside the settings and the stuff is saved in some very ancient way (nice string lists of hand-crafted text output of colors/bools/...) inside KConfig INI files.
There is some import/export of this stuff based on KConfig INI files, too.

### Frameworks starting with version 5.75

Two years ago, we migrated our syntax highlighting to [KSyntaxHighlighting](/2018/08/14/porting-ktexteditor-to-ksyntaxhighlighting-done/).
That was some work done mostly at the [Akademy 2018](https://akademy.kde.org/2018).

What we didn't touch during that port: KSyntaxHighlighting has an own color theme concept that allows more or less all that we did support in KTextEditor but on the base of much better defined JSON .theme files.

This means you can now have a theme being self-contained in a small file and you need not to import it somewhere into a large configuration mess but just place it like a highlighting definition file in some folder in either the system-wide or user-wide XDG directory "org.kde.syntax-highlighting/themes".

If you use KSyntaxHighlighting or KTextEditor in your own application, you can even just bundle these themes as Qt resources in ":/org.kde.syntax-highlighting/themes" inside your application binary. This simplifies bundling of own themes a lot.

Starting with Frameworks version 5.75 we will start to make use of this variant of color themes.
KSyntaxHighlighting themes now show up as color themes inside KTextEditor based applications like Kate and are usable out of the box.
The change was done in [this merge request](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/20).

The UI is changed to call this "Color Theme" instead of "Schema", the font choosing is decoupled from this now in the settings dialog.
Color themes can only alter the coloring and text attributes like bold/italic/underline/..., but not switch the font itself.

Per default the default color theme for KTextEditor based applications in now automatically selected based on the configured KDE color theme,
e.g. for dark themes you will get a dark theme per default, for light themes a light one.

You still can just set the theme you want as default and it will be enforced like before, but I think the automatic selection variant is much nicer any new user.
Not longer a "white" Kate editor widget if the user starts to use a "Breeze Dark" KDE theme ;)

As the automatic selection uses the Qt application palette to decide which theme to use, this should work on Windows and macOS, too

What still needs to be done is saving user modification as JSON .theme files.
At the moment this still goes back into the old KConfig INI files.

The "Export..." feature already allows to create JSON files (with the global color/style settings, without settings specific for individual highlighting definitions) if one selects the ending ".theme" for the file to save to.

We try to preserve user configured settings as good as possible, but I assume there will be some disruptions.

Already the first step required some internal renaming code and for sure there are cases where some settings are lost.

But I hope that the new solution will be a lot more stable in that respect.

The JSON file format should stay compatible, just like our XML syntax highlighting format is more or less compatible since more than one decade.

The documentation is still lacking, but given the format is very easy and all keys should be self-explaining, learning by example should be a good enough start.

We started to add more bundled color themes, too, like [Dracula](https://draculatheme.com/), added [here](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/58).
This might make for a good example how to contribute own themes for bundling.

All bundled themes are show cased on our [color themes page](/themes/).

That theme was created using the "Export..." functionality in the settings UI (choose ".theme" as file name ending).

More ideas which themes might be nice can be found/collected [here](https://invent.kde.org/frameworks/syntax-highlighting/-/issues/2).

### The future...

I hope that we arrive in the near future at the state that we directly save all configuration as .theme files without any additional INI file stuff.

This will again lead to some UI changes (e.g. you will not overwrite existing system shipped themes but clone them as new local ones that then are saved).

I guess that will be the step that definitively looses even more compatibility with the old INI file settings people have around.
Perhaps an import tool for old stuff is feasible.

Help is always welcome to make this transition more smooth!

See e.g. the [contributing via merge requests post](/post/2020/2020-07-18-contributing-via-gitlab-merge-requests/) how to contribute to our stuff.

We are happy to receive merge requests that help to transit our code to pure KSyntaxHighlighting theme usage and to get more popular MIT licensed bundled themes.
