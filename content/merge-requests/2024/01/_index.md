---
title: Merge Requests - January 2024
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2024/01/
---

#### Week 05

- **Kate - [Diagnostic limit handling improvements](https://invent.kde.org/utilities/kate/-/merge_requests/1394)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

#### Week 04

- **KTextEditor - [Add parent widget for diff dialogs](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/662)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged at creation day.

- **KTextEditor - [buffer/katesecuretextbutter: conform to KAuth api](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/661)**<br />
Request authored by [Yifan Zhu](https://invent.kde.org/fanzhuyifan) and merged at creation day.

- **KTextEditor - [Allow disabling &#x27;enter to insert completion&#x27;](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/660)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **Kate - [dap/client.cpp: Use QJsonObject instead of null](https://invent.kde.org/utilities/kate/-/merge_requests/1391)**<br />
Request authored by [Akseli Lahtinen](https://invent.kde.org/akselmo) and merged at creation day.

#### Week 03

- **KTextEditor - [Fix undo broken after pasting a line with wrap in the middle](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/659)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Introduce ArgumentHintWidget](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/658)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

#### Week 02

- **Kate - [KateSaveModifiedDialog: Play warning message box sound](https://invent.kde.org/utilities/kate/-/merge_requests/1389)**<br />
Request authored by [Kai Uwe Broulik](https://invent.kde.org/broulik) and merged at creation day.

- **Kate - [don&#x27;t build ktexteditor, doesn&#x27;t match the sdk anymore](https://invent.kde.org/utilities/kate/-/merge_requests/1390)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KSyntaxHighlighting - [Odin language syntax highlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/598)**<br />
Request authored by [Akseli Lahtinen](https://invent.kde.org/akselmo) and merged at creation day.

- **Kate - [More no-PCH fixes](https://invent.kde.org/utilities/kate/-/merge_requests/1388)**<br />
Request authored by [Sam James](https://invent.kde.org/thesamesam) and merged at creation day.

- **Kate - [[23.08] Fix no-PCH build](https://invent.kde.org/utilities/kate/-/merge_requests/1387)**<br />
Request authored by [Sam James](https://invent.kde.org/thesamesam) and merged at creation day.

- **Kate - [Improve clippy tool](https://invent.kde.org/utilities/kate/-/merge_requests/1385)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [lsp: Dont assume we always have percentage in progress notification](https://invent.kde.org/utilities/kate/-/merge_requests/1386)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [start to redo buffer to store more stuff inline in the blocks](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/657)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 3 days.

- **KSyntaxHighlighting - [Update doxyfile.xml to Doxygen 1.10.0](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/597)**<br />
Request authored by [Jeremy Murphy](https://invent.kde.org/jmurphy) and merged at creation day.

#### Week 01

- **Kate - [Remove setUpdate KBookmarkManager call](https://invent.kde.org/utilities/kate/-/merge_requests/1384)**<br />
Request authored by [Sune Vuorela](https://invent.kde.org/sune) and merged at creation day.

- **Kate - [Readd flatpak CI](https://invent.kde.org/utilities/kate/-/merge_requests/1383)**<br />
Request authored by [Laurent Montel](https://invent.kde.org/mlaurent) and merged after 3 days.

- **KTextEditor - [Store text lines inline](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/654)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **KTextEditor - [Remove unneeded check from defaultStyleAt](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/656)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [katedocument: Avoid closing modified read-only tab](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/655)**<br />
Request authored by [Eugene Popov](https://invent.kde.org/epopov) and merged at creation day.

- **KSyntaxHighlighting - [Convert shared_ptr to raw before use in the loop](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/595)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [Avoid wrapping QStringView with QStringView](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/596)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

