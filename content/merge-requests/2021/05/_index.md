---
title: Merge Requests - May 2021
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2021/05/
---

#### Week 21

- **Kate - [Work around conflicting shortcuts in preview plugin](https://invent.kde.org/utilities/kate/-/merge_requests/419)**<br />
Request authored by [Christoph Roick](https://invent.kde.org/croick) and merged after 2 days.

- **Kate - [lspclient: add action to request and apply quick fix code action](https://invent.kde.org/utilities/kate/-/merge_requests/421)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged at creation day.

- **Kate - [kateview*: Use more std containers and cleanup](https://invent.kde.org/utilities/kate/-/merge_requests/416)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 4 days.

- **Kate - [Prevent crash in git-blame for HUGE git commits](https://invent.kde.org/utilities/kate/-/merge_requests/420)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged at creation day.

- **Kate - [Small cmake cleanups of things no longer needed due to dep version bumps](https://invent.kde.org/utilities/kate/-/merge_requests/418)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged after one day.

- **Kate - [KateDocManager: Less manual memory handling and use more std stuff](https://invent.kde.org/utilities/kate/-/merge_requests/415)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 3 days.

- **Kate - [No manual memory management for katesession*](https://invent.kde.org/utilities/kate/-/merge_requests/413)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Fix crash when turning on &quot;bg shading&quot; for Documents plugin](https://invent.kde.org/utilities/kate/-/merge_requests/417)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Adjust completion parentheses behavior](https://invent.kde.org/utilities/kate/-/merge_requests/412)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after one day.

- **Kate - [S&amp;R: Fix matching ^ and $ in multi-line expressions](https://invent.kde.org/utilities/kate/-/merge_requests/414)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged at creation day.

- **KTextEditor - [Limit shortcut context to active view for Reuse Word Above/Below](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/155)**<br />
Request authored by [Christoph Roick](https://invent.kde.org/croick) and merged at creation day.

- **Kate - [Project context menu update](https://invent.kde.org/utilities/kate/-/merge_requests/375)**<br />
Request authored by [Krzysztof Stokop](https://invent.kde.org/mynewnickname) and merged after 28 days.

- **Kate - [Style LSP locations treeview for better readibility](https://invent.kde.org/utilities/kate/-/merge_requests/406)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 5 days.

- **Kate - [Reduce manual memory allocs and use more std stuff in katemdi](https://invent.kde.org/utilities/kate/-/merge_requests/411)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KSyntaxHighlighting - [Rust syntax - Separate ControlFlow from Keywords](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/199)**<br />
Request authored by [Amaury Bouchra Pilet](https://invent.kde.org/abp) and merged after one day.

#### Week 20

- **Kate - [S&amp;R: update highlight upon toggle of check state](https://invent.kde.org/utilities/kate/-/merge_requests/408)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after 2 days.

- **Kate - [Open Folder With: Kate](https://invent.kde.org/utilities/kate/-/merge_requests/409)**<br />
Request authored by [Thiago Sueto](https://invent.kde.org/thiagosueto) and merged at creation day.

- **Kate - [Clean up KateRunningInstanceInfo](https://invent.kde.org/utilities/kate/-/merge_requests/407)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [S&amp;R: mark handling](https://invent.kde.org/utilities/kate/-/merge_requests/405)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after one day.

- **KTextEditor - [Add &quot;Transpose Words&quot; feature](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/153)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after one day.

- **Kate - [Implement LSP semantic tokens protocol](https://invent.kde.org/utilities/kate/-/merge_requests/398)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 3 days.

- **KTextEditor - [Fix on-the-fly spell checking with recent Qt](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/154)**<br />
Request authored by [Antonio Rojas](https://invent.kde.org/arojas) and merged at creation day.

- **KSyntaxHighlighting - [Use more target-centric cmake code](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/198)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged at creation day.

- **Kate - [lspclient: only unset cursor if previously set](https://invent.kde.org/utilities/kate/-/merge_requests/403)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged at creation day.

- **Kate - [lspclient updates](https://invent.kde.org/utilities/kate/-/merge_requests/402)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged at creation day.

- **KSyntaxHighlighting - [Port away from legacy LINK_PUBLIC &amp; LINK_PRIVATE](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/197)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged at creation day.

#### Week 19

- **Kate - [Fix leak when staging lines / hunks](https://invent.kde.org/utilities/kate/-/merge_requests/399)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Make quickopen sizing like other hud dialogs](https://invent.kde.org/utilities/kate/-/merge_requests/401)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [fix quick open for untitled documents](https://invent.kde.org/utilities/kate/-/merge_requests/400)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [Add support for Vala language server](https://invent.kde.org/utilities/kate/-/merge_requests/397)**<br />
Request authored by [Pani Ram](https://invent.kde.org/ducksoft) and merged after one day.

- **KTextEditor - [Remove empty destructors &amp; port to std::vector](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/151)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 6 days.

- **Kate - [gitblame: add action to toggle showing of blame info](https://invent.kde.org/utilities/kate/-/merge_requests/396)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after one day.

- **Kate - [quickopen: Use std::vector and remove QFileInfo](https://invent.kde.org/utilities/kate/-/merge_requests/393)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 4 days.

- **Kate - [Prevent storing diff files in recent files history](https://invent.kde.org/utilities/kate/-/merge_requests/395)**<br />
Request authored by [Méven Car](https://invent.kde.org/meven) and merged after 2 days.

- **Kate - [Git: Allow amending last commit message](https://invent.kde.org/utilities/kate/-/merge_requests/388)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 6 days.

- **KTextEditor - [Fixup registering text hint providers](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/152)**<br />
Request authored by [David Redondo](https://invent.kde.org/davidre) and merged at creation day.

- **KSyntaxHighlighting - [Fix perl qw&lt; &gt; highlighting and extend test](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/196)**<br />
Request authored by [Daniel Sonck](https://invent.kde.org/dsonck) and merged at creation day.

- **Kate - [Simplify and modernize KateViewSpace::saveConfig](https://invent.kde.org/utilities/kate/-/merge_requests/394)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [LSP: Improve tooltip](https://invent.kde.org/utilities/kate/-/merge_requests/392)**<br />
Request authored by [Méven Car](https://invent.kde.org/meven) and merged after 2 days.

#### Week 18

- **KTextEditor - [Use std::vector for MovingRanges and TextHintProvider](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/150)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Use std::vector in WordCounter](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/149)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KSyntaxHighlighting - [Update GAS syntax to highlight reg/instrs/jmp instrs](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/195)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 8 days.

- **Kate - [Project git: add icon for refresh action in the menu](https://invent.kde.org/utilities/kate/-/merge_requests/389)**<br />
Request authored by [Méven Car](https://invent.kde.org/meven) and merged at creation day.

- **Kate - [Project git tree : Fix single file context menus action](https://invent.kde.org/utilities/kate/-/merge_requests/390)**<br />
Request authored by [Méven Car](https://invent.kde.org/meven) and merged at creation day.

- **KTextEditor - [Theme combo box: Add a separator line after &#x27;Automatic Selection&#x27;](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/147)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged at creation day.

- **Kate - [[addons/project/git/gitutils] Add missing include optional to make bsd happy](https://invent.kde.org/utilities/kate/-/merge_requests/387)**<br />
Request authored by [Ömer Fadıl Usta](https://invent.kde.org/usta) and merged at creation day.

- **Kate - [Fix branch change not reflected if projectbase != dotGit](https://invent.kde.org/utilities/kate/-/merge_requests/386)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [remove openheader plugin in favor of LSP](https://invent.kde.org/utilities/kate/-/merge_requests/384)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **Kate - [Fix leak in kategdbplugin](https://invent.kde.org/utilities/kate/-/merge_requests/383)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Misc quickopen fixes and adjustments](https://invent.kde.org/utilities/kate/-/merge_requests/381)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after one day.

- **Kate - [kate: show commandbar action in menu](https://invent.kde.org/utilities/kate/-/merge_requests/382)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged at creation day.

- **KTextEditor - [Fix leak in VariableLineEdit](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/146)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [project plugin: Use more forward decls](https://invent.kde.org/utilities/kate/-/merge_requests/380)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Remove dead code](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/145)**<br />
Request authored by [Hannah von Reth](https://invent.kde.org/vonreth) and merged at creation day.

#### Week 17

- **Kate - [lspclient: add a &quot;Code Action&quot; menu](https://invent.kde.org/utilities/kate/-/merge_requests/379)**<br />
Request authored by [Olivier Goffart](https://invent.kde.org/ogoffart) and merged at creation day.

- **KTextEditor - [less manual heap memory management in vimode](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/144)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KTextEditor - [Fix leak in KateCompletionWidget](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/143)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

