---
title: Merge Requests - December 2023
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2023/12/
---

#### Week 52

- **KTextEditor - [remove folding storage, compute that on the fly](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/653)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **Kate - [keep track when we got activated last time](https://invent.kde.org/utilities/kate/-/merge_requests/1381)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 3 days.

- **Kate - [Fix search result paths if file is in &quot;baseDir&quot;](https://invent.kde.org/utilities/kate/-/merge_requests/1379)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged at creation day.

- **KTextEditor - [Remove source code compat includes](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/651)**<br />
Request authored by [Alexander Lohnau](https://invent.kde.org/alex) and merged at creation day.

#### Week 51

- **Kate - [collapse url changed events](https://invent.kde.org/utilities/kate/-/merge_requests/1378)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [lsp: remove rootIndicationFileNames for rust](https://invent.kde.org/utilities/kate/-/merge_requests/1377)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [undo: Merge editInsertText+editWrapLine into editInsertLine](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/650)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Optimize textInsert/setText](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/649)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Add GitLab jobs to sign and optionally publish Microsoft APPX packages](https://invent.kde.org/utilities/kate/-/merge_requests/1375)**<br />
Request authored by [Ingo Klöcker](https://invent.kde.org/kloecker) and merged after one day.

- **Kate - [links: Fix links not highlighted when text removed or pasted](https://invent.kde.org/utilities/kate/-/merge_requests/1376)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [remove shortcut that clashes with font size change](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/648)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KTextEditor - [katemessagewidget: Use KMessageBox::Position](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/647)**<br />
Request authored by [Carl Schwan](https://invent.kde.org/carlschwan) and merged after one day.

#### Week 50

- **Kate - [diagnostic: Ensure filter line edit height is consistent](https://invent.kde.org/utilities/kate/-/merge_requests/1373)**<br />
Request authored by [Carl Schwan](https://invent.kde.org/carlschwan) and merged at creation day.

- **Kate - [search: Use margins from style instead of hardcoding values](https://invent.kde.org/utilities/kate/-/merge_requests/1374)**<br />
Request authored by [Carl Schwan](https://invent.kde.org/carlschwan) and merged at creation day.

- **Kate - [Limit diagnostics to 12000](https://invent.kde.org/utilities/kate/-/merge_requests/1369)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 11 days.

- **Kate - [buildplugin tweaks](https://invent.kde.org/utilities/kate/-/merge_requests/1370)**<br />
Request authored by [Karthik Nishanth](https://invent.kde.org/nishanthkarthik) and merged after 8 days.

- **KTextEditor - [GIT_SILENT Port to new way of including CI templates](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/646)**<br />
Request authored by [Albert Astals Cid](https://invent.kde.org/aacid) and merged at creation day.

- **KSyntaxHighlighting - [GIT_SILENT Port to new way of including CI templates](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/593)**<br />
Request authored by [Albert Astals Cid](https://invent.kde.org/aacid) and merged at creation day.

- **KSyntaxHighlighting - [systemd unit: update to systemd v255](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/592)**<br />
Request authored by [Andreas Gratzer](https://invent.kde.org/andreasgr) and merged after 7 days.

- **Kate - [Improve lsp semanticTokens range highlighting](https://invent.kde.org/utilities/kate/-/merge_requests/1372)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 3 days.

- **KTextEditor - [Remember scroll position in session](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/644)**<br />
Request authored by [Arthur Tadier](https://invent.kde.org/atadier) and merged after 2 days.

#### Week 49

- **Kate - [Add CI template for building APPX packages on invent](https://invent.kde.org/utilities/kate/-/merge_requests/1368)**<br />
Request authored by [Ingo Klöcker](https://invent.kde.org/kloecker) and merged after 4 days.

- **KSyntaxHighlighting - [Update known options for kdesrc-buildrc](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/590)**<br />
Request authored by [Andrew Shark](https://invent.kde.org/ashark) and merged after 4 days.

- **KSyntaxHighlighting - [cmake.xml: add CMake 3.28 features](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/591)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged at creation day.

- **KTextEditor - [Allow co-installability of the KAuth files with KF5](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/643)**<br />
Request authored by [Volker Krause](https://invent.kde.org/vkrause) and merged after one day.

- **kate-editor.org - [Setup Gitlab CI for kate-editor.org](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/59)**<br />
Request authored by [Ben Cooksley](https://invent.kde.org/bcooksley) and merged after one day.

#### Week 48

- **KTextEditor - [view/kateview.cpp (KTextEditor::ViewPrivate::setupActions) : Fix small typo in setWhatsThis text.](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/642)**<br />
Request authored by [Oliver Kellogg](https://invent.kde.org/okellogg) and merged at creation day.

- **KTextEditor - [same code for wayland](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/641)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 7 days.

- **KSyntaxHighlighting - [add support for viper language](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/589)**<br />
Request authored by [Karthik Nishanth](https://invent.kde.org/nishanthkarthik) and merged at creation day.

