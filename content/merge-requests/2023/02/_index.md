---
title: Merge Requests - February 2023
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2023/02/
---

#### Week 09

- **Kate - [Fix widgets not cleared on new session](https://invent.kde.org/utilities/kate/-/merge_requests/1133)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Save session manager sort column/order](https://invent.kde.org/utilities/kate/-/merge_requests/1131)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Fix Documents plugin name](https://invent.kde.org/utilities/kate/-/merge_requests/1132)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

#### Week 08

- **Kate - [Stash improvements / fixes](https://invent.kde.org/utilities/kate/-/merge_requests/1130)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Add Qt6 windows CI support](https://invent.kde.org/utilities/kate/-/merge_requests/1128)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **Kate - [More terminal fixes](https://invent.kde.org/utilities/kate/-/merge_requests/1129)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Allow splitting when activeView is widget](https://invent.kde.org/utilities/kate/-/merge_requests/992)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 116 days.

- **Kate - [Make formatters in formatting plugin configurable](https://invent.kde.org/utilities/kate/-/merge_requests/1125)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **Kate - [Lots of terminal fixes on windows](https://invent.kde.org/utilities/kate/-/merge_requests/1127)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Build and Search plugin fix](https://invent.kde.org/utilities/kate/-/merge_requests/1126)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after one day.

- **KTextEditor - [Remove QTextCodec](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/488)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **Kate - [Handle enabling/disabling of search action in SearchPlugin](https://invent.kde.org/utilities/kate/-/merge_requests/1088)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 25 days.

- **KTextEditor - [Clean up unused deprecation version numbers](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/487)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged at creation day.

- **Kate - [Diff: Allow expanding the context](https://invent.kde.org/utilities/kate/-/merge_requests/1123)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Fix Qt6 build](https://invent.kde.org/utilities/kate/-/merge_requests/1124)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Improve git push pull](https://invent.kde.org/utilities/kate/-/merge_requests/1122)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Add JSON language server configuration](https://invent.kde.org/utilities/kate/-/merge_requests/1120)**<br />
Request authored by [Alex Lowe](https://invent.kde.org/alexlowe) and merged at creation day.

- **Kate - [back to config dialog](https://invent.kde.org/utilities/kate/-/merge_requests/1119)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [ExternalTools: Fix tools with variables in their exec are ignored](https://invent.kde.org/utilities/kate/-/merge_requests/1108)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 4 days.

- **Kate - [build-plugin: If no selection select first](https://invent.kde.org/utilities/kate/-/merge_requests/1117)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged after one day.

- **KTextEditor - [indentation/ruby: indent method definitions having an access modifier](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/484)**<br />
Request authored by [Jyrki Gadinger](https://invent.kde.org/ggadinger) and merged after 2 days.

- **KTextEditor - [Explicitly link against Qt6::Core5Compat](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/486)**<br />
Request authored by [Volker Krause](https://invent.kde.org/vkrause) and merged after one day.

#### Week 07

- **Kate - [Ctags fixes/improvement](https://invent.kde.org/utilities/kate/-/merge_requests/1116)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Minimap: Dont jump if click is inside the slider](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/485)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [Adding Catppuccin themes](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/455)**<br />
Request authored by [M. Ibrahim](https://invent.kde.org/sourcastic) and merged after 5 days.

- **KSyntaxHighlighting - [dosbat: Add cmd extension](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/458)**<br />
Request authored by [Biswapriyo Nath](https://invent.kde.org/biswapriyo) and merged at creation day.

- **Kate - [Tabswitcher: Support widgets](https://invent.kde.org/utilities/kate/-/merge_requests/1114)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [MainWindow: Make activateView activate a view in another space if needed](https://invent.kde.org/utilities/kate/-/merge_requests/1113)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Allow changing shell program](https://invent.kde.org/utilities/kate/-/merge_requests/1112)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Sessions: Save session on document open and close](https://invent.kde.org/utilities/kate/-/merge_requests/1115)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [systemd unit: update to systemd v253](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/456)**<br />
Request authored by [Andreas Gratzer](https://invent.kde.org/andreasgr) and merged after one day.

- **KSyntaxHighlighting - [Clean up Qt Quick bindings](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/457)**<br />
Request authored by [Volker Krause](https://invent.kde.org/vkrause) and merged at creation day.

- **Kate - [Simplify names of konsole and snippets plugin](https://invent.kde.org/utilities/kate/-/merge_requests/1107)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **Kate - [Fix view management bugs](https://invent.kde.org/utilities/kate/-/merge_requests/1110)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Save global options of active window before creating new](https://invent.kde.org/utilities/kate/-/merge_requests/1109)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Enable dark theme for title bar in Windows.](https://invent.kde.org/utilities/kate/-/merge_requests/1111)**<br />
Request authored by [Nicholas Omann](https://invent.kde.org/nicholasomann) and merged at creation day.

- **KSyntaxHighlighting - [make Repository an QObject, emit signals on reload](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/442)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 16 days.

- **KSyntaxHighlighting - [impr(toml.yaml): table key style now separate from header](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/451)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged after 6 days.

- **Kate - [Dont try to clear prompt on windows](https://invent.kde.org/utilities/kate/-/merge_requests/1106)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **kate-editor.org - [Kate windows terminal post](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/54)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Introduce new terminal that works on windows/mac/linux](https://invent.kde.org/utilities/kate/-/merge_requests/1104)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 4 days.

- **Kate - [Clear file2Item on project reload](https://invent.kde.org/utilities/kate/-/merge_requests/1105)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KSyntaxHighlighting - [inline some function of StateData and takes captures by rvalue (highlighter_benchmark 2.5% faster)](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/454)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

#### Week 06

- **Kate - [project pluign: make the &quot;list&quot; mode work again](https://invent.kde.org/utilities/kate/-/merge_requests/1102)**<br />
Request authored by [Alexander Neundorf](https://invent.kde.org/neundorf) and merged after 3 days.

- **KSyntaxHighlighting - [impr(alerts.xml): recognize `noqa:` used by some (Python?) linters](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/452)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged after one day.

- **KSyntaxHighlighting - [Update SO version to 6, by explicitly setting it in ecm_setup_version()](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/453)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged after one day.

- **kate-editor.org - [Retrieve the artifact link from Binary Factory](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/53)**<br />
Request authored by [Phu Nguyen](https://invent.kde.org/phunh) and merged at creation day.

- **kate-editor.org - [Write a kate plugin page - Point to dev.kde resource and warn](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/52)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 11 days.

- **Kate - [docs: fix lspclient example configuration](https://invent.kde.org/utilities/kate/-/merge_requests/1103)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after one day.

- **Kate - [Only restore views for first main window](https://invent.kde.org/utilities/kate/-/merge_requests/1101)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [LspInlayHints: Unregister note provider on destruction](https://invent.kde.org/utilities/kate/-/merge_requests/1100)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Some improvements for diffwidget](https://invent.kde.org/utilities/kate/-/merge_requests/1099)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Konsole: Dont cd if a process is running in terminal](https://invent.kde.org/utilities/kate/-/merge_requests/1098)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [cmake.xml: add features of CMake 3.26](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/450)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged at creation day.

- **KTextEditor - [Work/alex/desktoptojson leftovers](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/483)**<br />
Request authored by [Alexander Lohnau](https://invent.kde.org/alex) and merged at creation day.

- **Kate - [project: Remove/delete standard items async when refreshing project](https://invent.kde.org/utilities/kate/-/merge_requests/1097)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [QML: add shebang](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/446)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **KSyntaxHighlighting - [C: add C23 keywords and uwb integer suffix](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/449)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [fast character search for AnyChar (highlighter_benchmark 1.2% faster)](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/447)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **KSyntaxHighlighting - [Indexer: check that &lt;Int&gt; is after &lt;Float&gt;](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/448)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

#### Week 05

- **Kate - [diff fixes](https://invent.kde.org/utilities/kate/-/merge_requests/1096)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [quickopen improvements](https://invent.kde.org/utilities/kate/-/merge_requests/1095)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [LSP tweaks](https://invent.kde.org/utilities/kate/-/merge_requests/1093)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after one day.

- **Kate - [lspclient: add default configuration for esbonio](https://invent.kde.org/utilities/kate/-/merge_requests/1094)**<br />
Request authored by [Alex Carney](https://invent.kde.org/alcarney) and merged at creation day.

- **KSyntaxHighlighting - [Spice: fix comments and various improvements](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/445)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **KSyntaxHighlighting - [OpenSCAD: detect regions, strings, numbers, use `dsKeyword` style for keywords](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/444)**<br />
Request authored by [Jyrki Gadinger](https://invent.kde.org/ggadinger) and merged after 2 days.

- **KSyntaxHighlighting - [dockerfile: add Containerfile as additional extension](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/443)**<br />
Request authored by [Jyrki Gadinger](https://invent.kde.org/ggadinger) and merged at creation day.

- **Kate - [gdbvariableparser scope of the variable &#x27;end&#x27; can be reduced](https://invent.kde.org/utilities/kate/-/merge_requests/1092)**<br />
Request authored by [Marius Pa](https://invent.kde.org/nmariusp) and merged at creation day.

- **KTextEditor - [Remove Qt 5 support](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/482)**<br />
Request authored by [Volker Krause](https://invent.kde.org/vkrause) and merged after 2 days.

- **KSyntaxHighlighting - [Add stopEmptyLineContextSwitchLoop attribute in &lt;context&gt; (#23)](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/440)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **KSyntaxHighlighting - [Clojure: Remove Color bracket hack](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/441)**<br />
Request authored by [shenleban tongying](https://invent.kde.org/slbtongying) and merged at creation day.

- **kate-editor.org - [fix bold in last blog and update multicursor blog](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/51)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

